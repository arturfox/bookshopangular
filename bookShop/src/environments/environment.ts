export const environment = {
  production: false
};

export const SERVER_URL = 'https://localhost:44356/';

//filters
export const MIN_PRICE_VALUE = 0;
export const MAX_PRICE_VALUE = 1000;
export const MAX_ITEMS_COUNT_PER_PAGE = 20;
export const SEARCH_DELAY_MILLISECONDS = 750;
export const TOAST_DURATION_MILLISECONDS = 3000;

export const stateKeys = ['account.currentUser'];
export const storageKey = 'app_storage';