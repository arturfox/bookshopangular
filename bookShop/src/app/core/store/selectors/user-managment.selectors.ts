import { createSelector } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { IUserManagmentState } from '../state/user-managment.state';

const userManagmentState = (state: IAppState) => state.userManagment;

export const selectUsers = createSelector(
    userManagmentState,
    (state: IUserManagmentState) =>{
        if(state !== null && state.users !== null){
            return state.users.users; 
        }        
        return null;
    }
);

export const selectPagination = createSelector(
    userManagmentState,
    (state: IUserManagmentState) =>{
        if(state !== null && state.users !== null){
            return { itemsPerPage: state.users.itemsPerPage, 
                currentPage: state.users.currentPage, totalItems: state.users.totalItems, id: "first"}; 
        }
        
        return null;
    }
);