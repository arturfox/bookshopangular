import { createSelector } from '@ngrx/store';

import { IAppState } from '../state/app.state';
import { ICatalogState } from '../state/catalog.state';

const catalogState = (state: IAppState) => state.catalog;

export const selectCatalog = createSelector(
    catalogState,
    (state: ICatalogState) =>{

        if(state !== null && state.catalog !== null){
            return state.catalog.items; 
        }
        
        return null;
    }
);

export const selectPagination = createSelector(
    catalogState,
    (state: ICatalogState) =>{

        if(state !== null && state.catalog !== null){
            return { itemsPerPage: state.catalog.itemsPerPage, currentPage: state.catalog.currentPage, totalItems: state.catalog.totalItems, id: "first"}; 
        }
        
        return null;
    }
);

export const selectCatalogDetails = createSelector(
    catalogState,
    (state: ICatalogState) =>{

        if(state !== null && state.details !== null){
            return state.details.data; 
        }
        
        return null;
    }
);