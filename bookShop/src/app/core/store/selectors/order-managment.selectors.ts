import { createSelector } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { IOrderManagmentState } from '../state/order-managment.state';

const orderManagmentState = (state: IAppState) => state.orderManagment;

export const selectOrders = createSelector(
    orderManagmentState,
    (state: IOrderManagmentState) =>{
        if(state !== null && state.orders !== null){
            return state.orders.orders; 
        }        
        return null;
    }
);

export const selectPagination = createSelector(
    orderManagmentState,
    (state: IOrderManagmentState) =>{

        if(state !== null && state.orders !== null){
            return { itemsPerPage: state.orders.itemsPerPage, currentPage: state.orders.currentPage, totalItems: state.orders.totalItems, id: "ordermanagment"}; 
        }
        
        return null;
    }
);