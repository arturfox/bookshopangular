import { createSelector } from '@ngrx/store';

import { IAppState } from '../state/app.state';
import { IAccountState } from '../state/account.state';

const accountState = (state: IAppState) => state.account;

export const selectAccount = createSelector(
    accountState,
    (state: IAccountState) => state.currentUser
);

export const selectRole = createSelector(
    accountState,
    (state: IAccountState) => { 
        if (state !== null && state.currentUser !== null){
            return state.currentUser.userRole;
        }

        return null;
    }
);

export const selectAccessToken = createSelector(
    accountState,
    (state: IAccountState) => {
        if(state !== null && state.currentUser != null){
            return state.currentUser.token; 
        }
        
        return null;
    }
);

export const selectRefreshToken = createSelector(
    accountState,
    (state: IAccountState) => {
        if(state !== null && state.currentUser != null){
            return state.currentUser.refreshToken; 
        }
        
        return null;
    }
);

export const isPasswordRecovered = createSelector(
    accountState,
    (state: IAccountState) => {     
        return (state != null && state.isPasswordRecovered);
    }
);

export const selectProfile = createSelector(
    accountState,
    (state: IAccountState) => state.profile
);
