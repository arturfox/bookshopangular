import { createSelector } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { ICatalogManagmentState } from '../state/catalog-managment-state';

const catalogManagmentState = (state: IAppState) => state.catalogManagment;

export const selectCatalog = createSelector(
    catalogManagmentState,
    (state: ICatalogManagmentState) => {
        if (state !== null && state.items !== null) {
            return state.items.items;
        }
        return null;
    }
);

export const selectPagination = createSelector(
    catalogManagmentState,
    (state: ICatalogManagmentState) => {
        if (state !== null && state.items !== null) {
            return {
                itemsPerPage: state.items.itemsPerPage,
                currentPage: state.items.currentPage,
                totalItems: state.items.totalItems,
                id: "catalogmanagment"
            };
        }

        return null;
    }
);

export const selectAllAuthors = createSelector(
    catalogManagmentState,
    (state: ICatalogManagmentState) => {
        if (state !== null && state.allAuthors !== null) {
            return state.allAuthors;
        }
        return null;
    }
);