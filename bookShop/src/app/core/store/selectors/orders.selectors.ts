import { createSelector } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { IOrdersState } from '../state/orders.state';

const ordersState = (state: IAppState) => state.orders;

export const selectOrders = createSelector(
    ordersState,
    (state: IOrdersState) =>{

        if(state !== null && state.orders !== null){
            return state.orders.orders; 
        }
        
        return null;
    }
);