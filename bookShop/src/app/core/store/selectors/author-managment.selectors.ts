import { createSelector } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { IAuthorManagmentState } from '../state/author-managment.state';

const authorManagmentState = (state: IAppState) => state.authorManagment;

export const selectAuthors= createSelector(
    authorManagmentState,
    (state: IAuthorManagmentState) =>{
        if(state !== null && state.authors !== null){
            return state.authors.items; 
        }        
        return null;
    }
);

export const selectPagination = createSelector(
    authorManagmentState,
    (state: IAuthorManagmentState) =>{

        if(state !== null && state.authors !== null){
            return { itemsPerPage: state.authors.itemsPerPage, currentPage: state.authors.currentPage, totalItems: state.authors.totalItems, id: "authormanagment"}; 
        }
        
        return null;
    }
);