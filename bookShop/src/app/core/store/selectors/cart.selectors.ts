import { createSelector } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { ICartState } from '../state/cart.state';

const cartState = (state: IAppState) => state.cart;

export const selectCart = createSelector(
    cartState,
    (state: ICartState) =>{

        if(state !== null && state.cart !== null){
            return state.cart; 
        }
        
        return null;
    }
);

export const selectCartPositionsCount = createSelector(
    cartState,
    (state: ICartState) =>{

        if(state !== null && state.cart !== null && state.cart.items.length !== 0){
            return state.cart.items.length.toString(); 
        }
        
        return null;
    }
);

export const hasPositions= createSelector(
    cartState,
    (state: ICartState) =>{

        if(state !== null && state.cart !== null
            && state.cart.items !== null
            && state.cart.items.length !== 0 ){
            return true; 
        }
        
        return false;
    }
);

export const isAddedToCart= createSelector(
    cartState,
    (state: ICartState, props: {printingEditionId: number}) =>{

        if(state !== null && state.cart !== null
            && state.cart.items !== null
            && state.cart.items.length !== 0 ){

                let position = state.cart.items.find(x => x.printingEditionId === props.printingEditionId);

            return (position != null); 
        }
        
        return false;
    }
);