import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ToastService } from '../../services/toast.service';
import { GetOrders, EOrderManagmentActions, GetOrdersFailure, GetOrdersSuccess } from '../actions/order-managment.actions';
import { OrderManagmentService } from '../../services/order-managment.service';
import { OrdersModel } from '../../models/api/order-managment/orders-model';


@Injectable()
export class OrderManagmentEffects {

  constructor(
    private _orderManagmentService: OrderManagmentService,
    private _actions$: Actions,
    private _toastService: ToastService) { }

  @Effect()
  getOrders$ = this._actions$.pipe(
    ofType<GetOrders>(EOrderManagmentActions.GetOrders),
    switchMap((action) =>
      this._orderManagmentService.GetOrders(action.payload) 
    ),
    switchMap((result) => {
      if (result.isSucceed) {
        return of(new GetOrdersSuccess(result.data));
      }

      this._toastService.showMessageFromResult<OrdersModel>(result);
      return of(new GetOrdersFailure());
    })
  );
}