import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { of, zip } from 'rxjs';
import { switchMap, withLatestFrom } from 'rxjs/operators';
import { ToastService } from '../../services/toast.service';
import { UserManagmentService } from '../../services/user-managment.service';
import {
  GetUsers,
  EUserManagmentActions,
  GetUsersSuccess,
  GetUsersFailure,
  SuspendUserAccount,
  SuspendUserAccountSuccess,
  SuspendUserAccountFailure,
  ActivateUserAccount,
  ActivateUserAccountSuccess,
  ActivateUserAccountFailure,
  RemoveUser,
  RemoveUserSuccess,
  RemoveUserFailure,
  UpdateUser,
  UpdateUserSuccess,
  UpdateUserFailure,
  /*SuspendUserAccountSuccessGetUsers*/
} from '../actions/user-managment.actions';
import { UsersModel } from '../../models/api/user-managment/users-model';
import { Store } from '@ngrx/store';
import { IAppState } from '../state/app.state';

@Injectable()
export class UserManagmentEffects {

  constructor(
    private _userManagmentService: UserManagmentService,
    private _actions$: Actions,
    private _toastService: ToastService,
    private _store: Store<IAppState>) { }

  @Effect()
  getUsers$ = this._actions$.pipe(
    ofType<GetUsers>(EUserManagmentActions.GetUsers),
    switchMap((action) =>
      zip(this._userManagmentService.GetUsers(action.payload), of(action.payload))//throw foward action's payload(query data) 
    ),
    switchMap((result) => {
      if ((result[0])['isSucceed']) {
        return of(new GetUsersSuccess({ data: (result[0])['data'], query: result[1] }));
      }

      this._toastService.showMessageFromResult<UsersModel>(result[0]);
      return of(new GetUsersFailure(result[1]!));
    })
  );


  //todo: fix perfomance issue in effects where returns multiple actions - suspendUserAccount$, activateUserAccount$, removeUser$
  //issue - https://medium.com/@maxime1992/hi-austin-thanks-for-the-article-5bbf01c92eb6, https://twitter.com/MikeRyanDev/status/938108618133602304
  //already tried - https://www.npmjs.com/package/ngrx-batch-action-reducer, https://www.npmjs.com/package/redux-batched-actions
  @Effect()
  suspendUserAccount$ = this._actions$.pipe(
    ofType<SuspendUserAccount>(EUserManagmentActions.SuspendUserAccount),
    switchMap((action) => this._userManagmentService.SuspendUserAccount(action.payload)),
    withLatestFrom(this._store),
    switchMap((resultWithState => {
      if (resultWithState["0"].isSucceed) {
        return of(
          new SuspendUserAccountSuccess(),
          new GetUsers(resultWithState["1"].userManagment.lastExecutedQuery)
        );
      }

      this._toastService.showMessageFromResult(resultWithState["0"]);
      return of(new SuspendUserAccountFailure());
    }))
  );

  @Effect()
  activateUserAccount$ = this._actions$.pipe(
    ofType<ActivateUserAccount>(EUserManagmentActions.ActivateUserAccount),
    switchMap((action) => this._userManagmentService.ActivateUserAccount(action.payload)),
    withLatestFrom(this._store),
    switchMap((resultWithState => {
      if (resultWithState["0"].isSucceed) {
        return of(
          new ActivateUserAccountSuccess(),
          new GetUsers(resultWithState["1"].userManagment.lastExecutedQuery)
        );
      }

      this._toastService.showMessageFromResult(resultWithState["0"]);
      return of(new ActivateUserAccountFailure());
    }))
  );

  @Effect()
  removeUser$ = this._actions$.pipe(
    ofType<RemoveUser>(EUserManagmentActions.RemoveUser),
    switchMap((action) => this._userManagmentService.RemoveUser(action.payload)),
    withLatestFrom(this._store),
    switchMap((resultWithState => {
      if (resultWithState["0"].isSucceed) {
        return of(
          new RemoveUserSuccess(),
          new GetUsers(resultWithState["1"].userManagment.lastExecutedQuery)
        );
      }

      this._toastService.showMessageFromResult(resultWithState["0"]);
      return of(new RemoveUserFailure());
    }))
  );

  @Effect()
  updateUser$ = this._actions$.pipe(
    ofType<UpdateUser>(EUserManagmentActions.UpdateUser),
    switchMap((action) => this._userManagmentService.UpdateUser(action.payload)),
    withLatestFrom(this._store),
    switchMap((resultWithState => {
      if (resultWithState["0"].isSucceed) {
        return of(
          new UpdateUserSuccess(),
          new GetUsers(resultWithState["1"].userManagment.lastExecutedQuery)
        );
      }

      this._toastService.showMessageFromResult(resultWithState["0"]);
      return of(new UpdateUserFailure());
    }))
  );
}