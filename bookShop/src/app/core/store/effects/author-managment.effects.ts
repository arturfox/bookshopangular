import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { of, zip } from 'rxjs';
import { switchMap, withLatestFrom } from 'rxjs/operators';
import { ToastService } from '../../services/toast.service';
import { Store } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { AuthorManagmentService } from '../../services/author-managment.service';
import { GetAuthors, GetAuthorsSuccess, EAuthorManagmentActions, GetAuthorsFailure, RemoveAuthor, RemoveAuthorSuccess, RemoveAuthorFailure, AddAuthor, AddAuthorSuccess, AddAuthorFailure, UpdateAuthor, UpdateAuthorSuccess, UpdateAuthorFailure } from '../actions/author-managment.actions';
import { AuthorsModel } from '../../models/api/author-managment/authors-model';

@Injectable()
export class AuthorManagmentEffects {

  constructor(
    private _authorManagmentService: AuthorManagmentService,
    private _actions$: Actions,
    private _toastService: ToastService,
    private _store: Store<IAppState>) { }

  @Effect()
  getAuthors$ = this._actions$.pipe(
    ofType<GetAuthors>(EAuthorManagmentActions.GetAuthors),
    switchMap((action) =>
      zip(this._authorManagmentService.GetAuthors(action.payload), of(action.payload))//throw foward action's payload(query data) 
    ),
    switchMap((result) => {
      if ((result[0])['isSucceed']) {
        return of(new GetAuthorsSuccess({ data: (result[0])['data'], query: result[1] }));
      }

      this._toastService.showMessageFromResult<AuthorsModel>(result[0]);
      return of(new GetAuthorsFailure(result[1]!));
    })
  );

  @Effect()
  removeAuthor$ = this._actions$.pipe(
    ofType<RemoveAuthor>(EAuthorManagmentActions.RemoveAuthor),
    switchMap((action) => this._authorManagmentService.RemoveAuthor(action.payload)),
    withLatestFrom(this._store),
    switchMap((resultWithState => {
      if (resultWithState["0"].isSucceed) {
        return of(
          new RemoveAuthorSuccess(),
          new GetAuthors(resultWithState["1"].authorManagment.lastExecutedQuery)
        );
      }

      this._toastService.showMessageFromResult(resultWithState["0"]);
      return of(new RemoveAuthorFailure());
    }))
  );
  
  @Effect()
  addAuthor$ = this._actions$.pipe(
    ofType<AddAuthor>(EAuthorManagmentActions.AddAuthor),
    switchMap((action) => this._authorManagmentService.AddAuthor(action.payload)),
    withLatestFrom(this._store),
    switchMap((resultWithState => {
      if (resultWithState["0"].isSucceed) {
        return of(
          new AddAuthorSuccess(),
          new GetAuthors(resultWithState["1"].authorManagment.lastExecutedQuery)
        );
      }

      this._toastService.showMessageFromResult(resultWithState["0"]);
      return of(new AddAuthorFailure());
    }))
  );

  @Effect()
 updateAuthor$ = this._actions$.pipe(
    ofType<UpdateAuthor>(EAuthorManagmentActions.UpdateAuthor),
    switchMap((action) => this._authorManagmentService.UpdateAuthor(action.payload)),
    withLatestFrom(this._store),
    switchMap((resultWithState => {
      if (resultWithState["0"].isSucceed) {
        return of(
          new UpdateAuthorSuccess(),
          new GetAuthors(resultWithState["1"].authorManagment.lastExecutedQuery)
        );
      }

      this._toastService.showMessageFromResult(resultWithState["0"]);
      return of(new UpdateAuthorFailure());
    }))
  );
}