import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ResultModel } from '../../models/api/result-model';
import {
  GetCatalog,
  ECatalogActions,
  GetCatalogSuccess,
  GetCatalogFailure,
  GetCatalogDetails,
  GetCatalogDetailsSuccess,
  GetCatalogDetailsFailure,
}
  from '../actions/catalog.actions';
import { CatalogService } from '../../services/catalog.service';
import { CatalogModel } from '../../models/api/catalog/catalog-model';
import { CatalogDetailsModel } from '../../models/api/catalog/catalog-details-model';
import { ToastService } from '../../services/toast.service';

@Injectable()
export class CatalogEffects {

  constructor(
    private _catalogService: CatalogService,
    private _actions$: Actions,
    private _toastService: ToastService
  ) { }

  @Effect()
  getCatalog$ = this._actions$.pipe(
    ofType<GetCatalog>(ECatalogActions.GetCatalog),
    switchMap((action) => this._catalogService.GetCatalog(action.payload)),
    switchMap((result: ResultModel<CatalogModel>) => {
      if (result.isSucceed) {
        return of(new GetCatalogSuccess(result.data));
      }

      this._toastService.showMessageFromResult<CatalogModel>(result);
      return of(new GetCatalogFailure());
    })
  );

  @Effect()
  getDetails$ = this._actions$.pipe(
    ofType<GetCatalogDetails>(ECatalogActions.GetCatalogDetails),
    switchMap((action) => this._catalogService.GetCatalogDetails(action.payload)),
    switchMap((result: ResultModel<CatalogDetailsModel>) => {
      if (result.isSucceed) {
        return of(new GetCatalogDetailsSuccess(result.data));
      }

      this._toastService.showMessageFromResult<CatalogDetailsModel>(result);
      return of(new GetCatalogDetailsFailure());
    })
  );
}