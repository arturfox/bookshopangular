import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { of, zip } from 'rxjs';
import { switchMap, withLatestFrom } from 'rxjs/operators';
import { ToastService } from '../../services/toast.service';
import { CatalogManagmentService } from '../../services/catalog-managment.service';
import { GetCatalog, ECatalogManagmentActions, GetCatalogFailure, GetCatalogSuccess, RemoveProduct, RemoveProductSuccess, RemoveProductFailure, GetAllAuthors, GetAllAuthorsSuccess, GetAllAuthorsFailure, AddProduct, AddProductSuccess, AddProductFailure, UpdateProduct, UpdateProductSuccess, UpdateProductFailure } from '../actions/catalog-managment.actions';
import { CatalogModel } from '../../models/api/catalog-managment/catalog-model';
import { Store } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { AuthorManagmentService } from '../../services/author-managment.service';
import { AllAuthorsModel } from '../../models/api/author-managment/all-authors-model';


@Injectable()
export class CatalogManagmentEffects {

  constructor(
    private _catalogManagmentService: CatalogManagmentService,
    private _authorManagmentService: AuthorManagmentService,
    private _actions$: Actions,
    private _toastService: ToastService,
    private _store: Store<IAppState>) { }

  @Effect()
  getCatalog$ = this._actions$.pipe(
    ofType<GetCatalog>(ECatalogManagmentActions.GetCatalog),
    switchMap((action) =>
      zip(this._catalogManagmentService.GetCatalog(action.payload), of(action.payload))//throw foward action's payload(query data) 
    ),
    switchMap((result) => {
      if ((result[0])['isSucceed']) {
        return of(new GetCatalogSuccess({ data: (result[0])['data'], query: result[1] }));
      }

      this._toastService.showMessageFromResult<CatalogModel>(result[0]);
      return of(new GetCatalogFailure(result[1]!));
    })
  );

  
  @Effect()
  removeProduct$ = this._actions$.pipe(
    ofType<RemoveProduct>(ECatalogManagmentActions.RemoveProduct),
    switchMap((action) => this._catalogManagmentService.RemoveProduct(action.payload)),
    withLatestFrom(this._store),
    switchMap((resultWithState => {
      if (resultWithState["0"].isSucceed) {
        return of(
          new RemoveProductSuccess(),
          new GetCatalog(resultWithState["1"].catalogManagment.lastExecutedQuery)
        );
      }

      this._toastService.showMessageFromResult(resultWithState["0"]);
      return of(new RemoveProductFailure());
    }))
  );

  @Effect()
  getAllAuthors$ = this._actions$.pipe(
    ofType<GetAllAuthors>(ECatalogManagmentActions.GetAllAuthors),
    switchMap((action) => this._authorManagmentService.GetAllAuthors()),
    switchMap((result) => {
      if (result.isSucceed) {
        return of(new GetAllAuthorsSuccess(result.data));
      }

      this._toastService.showMessageFromResult<AllAuthorsModel>(result);
      return of(new GetAllAuthorsFailure());
    })
  );

  @Effect()
  addProduct$ = this._actions$.pipe(
    ofType<AddProduct>(ECatalogManagmentActions.AddProduct),
    switchMap((action) => this._catalogManagmentService.AddProduct(action.payload)),
    withLatestFrom(this._store),
    switchMap((resultWithState => {
      if (resultWithState["0"].isSucceed) {
        return of(
          new AddProductSuccess(),
          new GetCatalog(resultWithState["1"].catalogManagment.lastExecutedQuery)
        );
      }

      this._toastService.showMessageFromResult(resultWithState["0"]);
      return of(new AddProductFailure());
    }))
  );

  @Effect()
  updateProduct$ = this._actions$.pipe(
    ofType<UpdateProduct>(ECatalogManagmentActions.UpdateProduct),
    switchMap((action) => this._catalogManagmentService.UpdateProduct(action.payload)),
    withLatestFrom(this._store),
    switchMap((resultWithState => {
      if (resultWithState["0"].isSucceed) {
        return of(
          new UpdateProductSuccess(),
          new GetCatalog(resultWithState["1"].catalogManagment.lastExecutedQuery)
        );
      }

      this._toastService.showMessageFromResult(resultWithState["0"]);
      return of(new UpdateProductFailure());
    }))
  );
}