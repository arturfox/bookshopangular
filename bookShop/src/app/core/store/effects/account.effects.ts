import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Login, LoginSuccess, EAccountActions, CreateAccount, CreateAccountSuccess, CreateAccountFailure, LoginByReference, LoginByReferenceSuccess, LoginFailure, RecoverPassword, RecoverPasswordSuccess, RecoverPasswordFailure, ResetIsPasswordRecovered, ResetIsPasswordRecoveredSuccess, Logout, LogoutSuccess, LogoutFailure, GetProfile, GetProfileSuccess, GetProfileFailure, UpdateProfile, UpdateProfileSuccess, UpdateProfileFailure } from '../actions/account.actions';
import { AccountService } from '../../services/account.service';
import { ResultModel } from '../../models/api/result-model';
import { LoginResultModel } from '../../models/api/authentication/login-result-model';
import { Account } from '../../models/account';
import { Router } from '@angular/router';
import { JwtHelper } from '../../helpers/jwt.helper';
import { ToastService } from '../../services/toast.service';
import { UserRole } from '../../enums/user-role.enum';
import { ProfileModel } from '../../models/api/authentication/profile-model';

@Injectable()
export class AccountEffects {

  constructor(
    private _accountService: AccountService,
    private _actions$: Actions,
    private _router: Router,
    private _jwtHelper: JwtHelper,
    private _toastService: ToastService
  ) { }

  @Effect()
  login$ = this._actions$.pipe(
    ofType<Login>(EAccountActions.Login),
    switchMap((action) => this._accountService.login(action.payload)),
    switchMap((result: ResultModel<LoginResultModel>) => {
      if (result.isSucceed) {

        let data = this._jwtHelper.getPayloadDataFromJWT(result.data.token);
        let account = new Account();
        account.token = result.data.token;
        account.refreshToken = result.data.refreshToken;
        account.firstName = data.given_name;
        account.lastName = data.family_name;
        account.userRole = UserRole[<string>data.role];
        account.isFirstLogin = false;

        return of(new LoginSuccess(account));
      }

      this._toastService.showMessageFromResult(result);

      return of(new LoginFailure());
    })
  );

  //todo: add token validation
  @Effect()
  loginByReference$ = this._actions$.pipe(
    ofType<LoginByReference>(EAccountActions.LoginByReference),
    switchMap((action) => {
      let data = this._jwtHelper.getPayloadDataFromJWT(action.payload.token);

      let account = new Account();
      account.token = action.payload.token;
      account.refreshToken = action.payload.token;
      account.firstName = data.given_name;
      account.lastName = data.family_name;
      account.userRole = UserRole[<string>data.role];
      account.isFirstLogin = true;

      return of(new LoginByReferenceSuccess(account));
    })
  );

  @Effect()
  createAccount$ = this._actions$.pipe(
    ofType<CreateAccount>(EAccountActions.CreateAccount),
    switchMap((action) => this._accountService.createAccount(action.payload)),
    switchMap((result: ResultModel) => {
      if (result.isSucceed) {
        this._router.navigateByUrl('createaccount/sucessfully');
        return of(new CreateAccountSuccess());
      }
     
      this._toastService.showMessageFromResult(result);

      return of(new CreateAccountFailure());
    })
  );

  //todo: clear all states, not only Account
  @Effect()
  logout$ = this._actions$.pipe(
    ofType<Logout>(EAccountActions.Logout),
    switchMap((action) => this._accountService.logout()),
    switchMap((result: ResultModel) => {

      this._router.navigateByUrl('login');

      if (result.isSucceed) {
        return of(new LogoutSuccess());
      }
     
      this._toastService.showMessageFromResult(result);
      return of(new LogoutFailure());
    })
  );

  @Effect()
  recoverPassword$ = this._actions$.pipe(
    ofType<RecoverPassword>(EAccountActions.RecoverPassword),
    switchMap((action) => this._accountService.recoverPassword(action.payload)),
    switchMap((result: ResultModel) => {
      if (result.isSucceed) {
        return of(new RecoverPasswordSuccess());
      }
  
      this._toastService.showMessageFromResult(result);

      return of(new RecoverPasswordFailure());
    })
  );

  @Effect()
  resetIsPasswordRecovered$ = this._actions$.pipe(
    ofType<ResetIsPasswordRecovered>(EAccountActions.ResetIsPasswordRecovered),
    switchMap((action) => {
        this._router.navigateByUrl('login');
        return of(new ResetIsPasswordRecoveredSuccess());
    })
  );

  @Effect()
  getProfile$ = this._actions$.pipe(
    ofType<GetProfile>(EAccountActions.GetProfile),
    switchMap((action) => this._accountService.GetProfile()),
    switchMap((result: ResultModel<ProfileModel>) => {
      if (result.isSucceed) {
        return of(new GetProfileSuccess(result.data));
      }

      this._toastService.showMessageFromResult(result);
      return of(new GetProfileFailure());
    })
  );

  @Effect()
  updateProfile$ = this._actions$.pipe(
    ofType<UpdateProfile>(EAccountActions.UpdateProfile),
    switchMap((action) => this._accountService.UpdateProfile(action.payload)),
    switchMap((result: ResultModel) => {
      if (result.isSucceed) {
        return of(new UpdateProfileSuccess());
      }

      this._toastService.showMessageFromResult(result);
      return of(new UpdateProfileFailure());
    })
  );
}