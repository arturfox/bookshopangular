import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ResultModel } from '../../models/api/result-model';
import { AddToCart, AddToCartSuccess, AddToCartFailure, ECartActions, GetCart, GetCartSuccess, GetCartFailure, RemoveFromCartSuccess, RemoveFromCartFailure, RemoveFromCart, Checkout, CheckoutSuccess, CheckoutFailure } from '../actions/cart.actions';
import { CartService } from '../../services/cart.service';
import { CartModel } from '../../models/api/cart/cart-model';
import { ToastService } from '../../services/toast.service';
import { CheckoutService } from '../../services/checkout.service';
import { CheckoutSessionModel } from '../../models/api/checkout/checkout-session-model';

@Injectable()
export class CartEffects {

  constructor(
    private _cartService: CartService,
    private _actions$: Actions,
    private _toastService: ToastService,
    private _checkoutService: CheckoutService
  ) { }

  @Effect()
  addToCart$ = this._actions$.pipe(
    ofType<AddToCart>(ECartActions.AddToCart),
    switchMap((action) => this._cartService.AddToCart(action.payload)),
    switchMap((result: ResultModel) => {
      if (result.isSucceed) {
        return of(new AddToCartSuccess());
      }
      
      this._toastService.showMessageFromResult(result); 
      return of(new AddToCartFailure());
    })
  );

  @Effect()
  getCart$ = this._actions$.pipe(
    ofType<GetCart>(ECartActions.GetCart),
    switchMap((action) => this._cartService.GetCart()),
    switchMap((result: ResultModel<CartModel>) => {
      if (result.isSucceed) {
        return of(new GetCartSuccess(result.data));
      }

      this._toastService.showMessageFromResult<CartModel>(result);
      return of(new GetCartFailure());
    })
  );

  @Effect()
  removePosition$ = this._actions$.pipe(
    ofType<RemoveFromCart>(ECartActions.RemoveFromCart),
    switchMap((action) => this._cartService.RemovePosition(action.payload)),
    switchMap((result: ResultModel) => {
      if (result.isSucceed) {
        return of(new RemoveFromCartSuccess());
      }

      this._toastService.showMessageFromResult(result);
      return of(new RemoveFromCartFailure());
    })
  );

  @Effect()
  removeFromCartSucceed$ = this._actions$.pipe(
    ofType<RemoveFromCartSuccess>(ECartActions.RemoveFromCartSuccess),
    switchMap((action) => 
    { 
      return of(new GetCart());
    }
  ))

  @Effect()
  addToCartSucceed$ = this._actions$.pipe(
    ofType<AddToCartSuccess>(ECartActions.AddToCartSuccess),
    switchMap((action) => 
    { 
      return of(new GetCart());
    }
  ))

  @Effect()
  checkout$ = this._actions$.pipe(
    ofType<Checkout>(ECartActions.Checkout),
    switchMap((action) => this._checkoutService.CreateCheckoutSession()),
    switchMap((result: ResultModel<CheckoutSessionModel>) => {
      if (result.isSucceed) {

        //
     stripe.redirectToCheckout({ sessionId: result.data.sessionId })
        .then(function (result) {
          // If redirectToCheckout fails due to a browser or network
          // error, you should display the localized error message to your
          // customer using error.message.
          if (result.error) {
            alert(result.error.message);
          }
        })
        .catch(function (error) {
          console.error("Error:", error);
        });   
       // 
        
        return of(new CheckoutSuccess());
      }

      this._toastService.showMessageFromResult<CheckoutSessionModel>(result);
      return of(new CheckoutFailure());
    })
  );

}