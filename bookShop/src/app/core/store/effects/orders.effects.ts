import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ResultModel } from '../../models/api/result-model';
import { ToastService } from '../../services/toast.service';
import { OrdersService } from '../../services/orders.service';
import { GetOrders, EOrdersActions, GetOrdersSuccess, GetOrdersFailure, Checkout, CheckoutSuccess } from '../actions/orders.actions';
import { OrdersModel } from '../../models/api/orders/orders-model';
import { CheckoutService } from '../../services/checkout.service';
import { CheckoutSessionModel } from '../../models/api/checkout/checkout-session-model';
import { CheckoutFailure } from '../actions/cart.actions';

@Injectable()
export class OrdersEffects {

    constructor(
        private _ordersService: OrdersService,
        private _actions$: Actions,
        private _toastService: ToastService,
        private _checkoutService: CheckoutService) { }

    @Effect()
    getOrders$ = this._actions$.pipe(
        ofType<GetOrders>(EOrdersActions.GetOrders),
        switchMap((action) => this._ordersService.GetOrders()),
        switchMap((result: ResultModel<OrdersModel>) => {
            if (result.isSucceed) {
                return of(new GetOrdersSuccess(result.data));
            }

            this._toastService.showMessageFromResult<OrdersModel>(result);
            return of(new GetOrdersFailure());
        })
    );


    @Effect()
    checkout$ = this._actions$.pipe(
      ofType<Checkout>(EOrdersActions.Checkout),
      switchMap((action) => this._checkoutService.CreateCheckoutSessionByOrderId(action.payload)),
      switchMap((result: ResultModel<CheckoutSessionModel>) => {
        if (result.isSucceed) {
  
          //
       stripe.redirectToCheckout({ sessionId: result.data.sessionId })
          .then(function (result) {
            // If redirectToCheckout fails due to a browser or network
            // error, you should display the localized error message to your
            // customer using error.message.
            if (result.error) {
              alert(result.error.message);
            }
          })
          .catch(function (error) {
            console.error("Error:", error);
          });   
         // 
          
          return of(new CheckoutSuccess());
        }
  
        this._toastService.showMessageFromResult<CheckoutSessionModel>(result);
        return of(new CheckoutFailure());
      })
    );
}