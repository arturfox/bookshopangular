import {Action} from '@ngrx/store';
import { OrdersModel } from '../../models/api/order-managment/orders-model';
import { GetOrdersModel } from '../../models/api/order-managment/get-orders-model';

export enum EOrderManagmentActions {
    GetOrders = '[OrderManagment] Get Orders',
    GetOrdersSuccess = '[OrderManagment] Get Orders Success',
    GetOrdersFailure = '[OrderManagment] Get Orders Failure',
}

export class GetOrders implements Action {
    public readonly type = EOrderManagmentActions.GetOrders;
    constructor(public payload: GetOrdersModel) {}
}

export class GetOrdersSuccess implements Action {
    public readonly type = EOrderManagmentActions.GetOrdersSuccess;
    constructor(public payload: OrdersModel) {}
}

export class GetOrdersFailure implements Action {
    public readonly type = EOrderManagmentActions.GetOrdersFailure;
    constructor() {}
}

export type OrderManagmentActions = GetOrders | GetOrdersSuccess | GetOrdersFailure;