import {Action} from '@ngrx/store';
import { AddToCartModel } from '../../models/api/cart/add-to-cart-model';
import { CartModel } from '../../models/api/cart/cart-model';
import { RemovePositionModel } from '../../models/api/cart/remove-position-model';

export enum ECartActions {
    AddToCart = '[Cart] Add To Cart',
    AddToCartSuccess = '[Cart] Add To Cart Success',
    AddToCartFailure = '[Cart] Add To Cart Failure',
    GetCart = '[Cart] Get Cart',
    GetCartSuccess = '[Cart] Get Cart Success',
    GetCartFailure = '[Cart] Get Cart Failure',
    RemoveFromCart = '[Cart] Remove From Cart',
    RemoveFromCartSuccess = '[Cart] Remove From Cart Success',
    RemoveFromCartFailure = '[Cart] Remove From Cart Failure',
    Checkout = '[Cart] Checkout',
    CheckoutSuccess = '[Cart] Checkout Success',
    CheckoutFailure = '[Cart] Checkout Failure',
}

export class AddToCart implements Action {
    public readonly type = ECartActions.AddToCart;
    constructor(public payload: AddToCartModel) {}
}

export class AddToCartSuccess implements Action {
    public readonly type = ECartActions.AddToCartSuccess;
    constructor() {}
}

export class AddToCartFailure implements Action {
    public readonly type = ECartActions.AddToCartFailure;
    constructor() {}
}

export class GetCart implements Action {
    public readonly type = ECartActions.GetCart;
    constructor() {}
}

export class GetCartSuccess implements Action {
    public readonly type = ECartActions.GetCartSuccess;
    constructor(public payload: CartModel) {}
}

export class GetCartFailure implements Action {
    public readonly type = ECartActions.GetCartFailure;
    constructor() {}
}

export class RemoveFromCart implements Action {
    public readonly type = ECartActions.RemoveFromCart;
    constructor(public payload: RemovePositionModel) {}
}

export class RemoveFromCartSuccess implements Action {
    public readonly type = ECartActions.RemoveFromCartSuccess;
    constructor() {}
}

export class RemoveFromCartFailure implements Action {
    public readonly type = ECartActions.RemoveFromCartFailure;
    constructor() {}
}

export class Checkout implements Action {
    public readonly type = ECartActions.Checkout;
    constructor() {}
}

export class CheckoutSuccess implements Action {
    public readonly type = ECartActions.CheckoutSuccess;
    constructor() {}
}

export class CheckoutFailure implements Action {
    public readonly type = ECartActions.CheckoutFailure;
    constructor() {}
}

export type CartActions = AddToCart | AddToCartSuccess | AddToCartFailure
                        | GetCart | GetCartSuccess | GetCartFailure
                        | RemoveFromCart | RemoveFromCartSuccess | RemoveFromCartFailure
                        | Checkout | CheckoutSuccess | CheckoutFailure;