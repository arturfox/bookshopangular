import {Action} from '@ngrx/store';
import { GetCatalogModel } from '../../models/api/catalog/get-catalog-model';
import { CatalogModel } from '../../models/api/catalog/catalog-model';
import { CatalogDetailsModel } from '../../models/api/catalog/catalog-details-model';

export enum ECatalogActions {
    GetCatalog = '[Catalog] Get Catalog',
    GetCatalogSuccess = '[Catalog] Get Catalog Success',
    GetCatalogFailure = '[Catalog] Get Catalog Failure',
    GetCatalogDetails = '[Catalog] Get Catalog Details',
    GetCatalogDetailsSuccess = '[Catalog] Get Catalog Details Success',
    GetCatalogDetailsFailure = '[Catalog] Get Catalog Details Failure'
}

export class GetCatalog implements Action {
    public readonly type = ECatalogActions.GetCatalog;
    constructor(public payload: GetCatalogModel) {}
}

export class GetCatalogSuccess implements Action {
    public readonly type = ECatalogActions.GetCatalogSuccess;
    constructor(public payload: CatalogModel) {}
}

export class GetCatalogFailure implements Action {
    public readonly type = ECatalogActions.GetCatalogFailure;
    constructor() {}
}

export class GetCatalogDetails implements Action {
    public readonly type = ECatalogActions.GetCatalogDetails;
    constructor(public payload: number) {}
}

export class GetCatalogDetailsSuccess implements Action {
    public readonly type = ECatalogActions.GetCatalogDetailsSuccess;
    constructor(public payload: CatalogDetailsModel) {}
}

export class GetCatalogDetailsFailure implements Action {
    public readonly type = ECatalogActions.GetCatalogDetailsFailure;
    constructor() {}
}

export type CatalogActions = GetCatalog | GetCatalogSuccess | GetCatalogFailure
                            | GetCatalogDetails | GetCatalogDetailsSuccess | GetCatalogDetailsFailure