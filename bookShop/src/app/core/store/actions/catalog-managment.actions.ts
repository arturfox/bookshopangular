import {Action} from '@ngrx/store';
import { GetCatalogModel } from '../../models/api/catalog-managment/get-catalog-model';
import { CatalogModel } from '../../models/api/catalog-managment/catalog-model';
import { AllAuthorsModel } from '../../models/api/author-managment/all-authors-model';
import { AddCatalogItemModel } from '../../models/api/catalog-managment/add-catalog-item-model';
import { UpdateCatalogItemModel } from '../../models/api/catalog-managment/update-catalog-item-model';

export enum ECatalogManagmentActions {
    GetCatalog = '[CatalogManagment] Get Catalog',
    GetCatalogSuccess = '[CatalogManagment] Get Catalog Success',
    GetCatalogFailure = '[CatalogManagment] Get Catalog Failure',
    RemoveProduct = '[CatalogManagment] Remove Product',
    RemoveProductSuccess = '[CatalogManagment] Remove Product Success',
    RemoveProductFailure = '[CatalogManagment] Remove Product Failure',
    GetAllAuthors = '[CatalogManagment] Get Authors',
    GetAllAuthorsSuccess = '[CatalogManagment] Get All Authors Success',
    GetAllAuthorsFailure = '[CatalogManagment] Get All Authors Failure',
    AddProduct = '[CatalogManagment] Add Product',
    AddProductSuccess = '[CatalogManagment] Add Product Success',
    AddProductFailure = '[CatalogManagment] Add Product Failure',
    UpdateProduct = '[CatalogManagment] Update Product',
    UpdateProductSuccess = '[CatalogManagment] Update Product Success',
    UpdateProductFailure = '[CatalogManagment] Update Product Failure',
}

export class GetCatalog implements Action {
    public readonly type = ECatalogManagmentActions.GetCatalog;
    constructor(public payload: GetCatalogModel) {}
}

export class GetCatalogSuccess implements Action {
    public readonly type = ECatalogManagmentActions.GetCatalogSuccess;
    constructor(public payload: { data: CatalogModel, query: GetCatalogModel }) {}
}

export class GetCatalogFailure implements Action {
    public readonly type = ECatalogManagmentActions.GetCatalogFailure;
    constructor(public payload: GetCatalogModel) {}
}

export class RemoveProduct implements Action {
    public readonly type = ECatalogManagmentActions.RemoveProduct;
    constructor(public payload: number) {}
}

export class RemoveProductSuccess implements Action {
    public readonly type = ECatalogManagmentActions.RemoveProductSuccess;
    constructor() {}
}

export class RemoveProductFailure implements Action {
    public readonly type = ECatalogManagmentActions.RemoveProductFailure;
    constructor() {}
}

export class GetAllAuthors implements Action {
    public readonly type = ECatalogManagmentActions.GetAllAuthors;
    constructor() { }
}

export class GetAllAuthorsSuccess implements Action {
    public readonly type = ECatalogManagmentActions.GetAllAuthorsSuccess;
    constructor(public payload: AllAuthorsModel) { }
}

export class GetAllAuthorsFailure implements Action {
    public readonly type = ECatalogManagmentActions.GetAllAuthorsFailure;
    constructor() { }
}

export class AddProduct implements Action {
    public readonly type = ECatalogManagmentActions.AddProduct;
    constructor(public payload: AddCatalogItemModel) {}
}

export class AddProductSuccess implements Action {
    public readonly type = ECatalogManagmentActions.AddProductSuccess;
    constructor() {}
}

export class AddProductFailure implements Action {
    public readonly type = ECatalogManagmentActions.AddProductFailure;
    constructor() {}
}

export class UpdateProduct implements Action {
    public readonly type = ECatalogManagmentActions.UpdateProduct;
    constructor(public payload: UpdateCatalogItemModel) {}
}

export class UpdateProductSuccess implements Action {
    public readonly type = ECatalogManagmentActions.UpdateProductSuccess;
    constructor() {}
}

export class UpdateProductFailure implements Action {
    public readonly type = ECatalogManagmentActions.UpdateProductFailure;
    constructor() {}
}


export type CatalogManagmentActions = GetCatalog | GetCatalogSuccess | GetCatalogFailure
                                    | RemoveProduct | RemoveProductSuccess | RemoveProductFailure
                                    | GetAllAuthors | GetAllAuthorsSuccess | GetAllAuthorsFailure
                                    | AddProduct | AddProductSuccess | AddProductFailure
                                    | UpdateProduct | UpdateProductSuccess | UpdateProductFailure;