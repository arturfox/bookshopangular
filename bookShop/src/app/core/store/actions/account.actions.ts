import {Action} from '@ngrx/store';
import {IAccount} from '../../models/account.interface';
import { LoginModel } from '../../models/api/authentication/login-model';
import { CreateAccountModel } from '../../models/api/authentication/create-account-model';
import { LoginResultModel } from '../../models/api/authentication/login-result-model';
import { RecoverPasswordModel } from '../../models/api/authentication/recover-password-model';
import { ProfileModel } from '../../models/api/authentication/profile-model';
import { UpdateProfileModel } from '../../models/api/authentication/update-profile-model';

export enum EAccountActions {
    Login = '[Account] Login',
    LoginSuccess = '[Account] Login Success',
    LoginFailure = '[Account] Login Failure',
    CreateAccount = '[Account] Create Account',
    CreateAccountSuccess = '[Account] Create Account Success',
    CreateAccountFailure = '[Account] Create Account Failure',
    LoginByReference = '[Account] Login By Reference',
    LoginByReferenceSuccess = '[Account] Login By Reference Success',
    RecoverPassword = '[Account] Recover Password',
    RecoverPasswordSuccess = '[Account] Recover Password Success',
    RecoverPasswordFailure = '[Account] Recover Password Failure',
    Logout = '[Account] Logout',
    LogoutSuccess = '[Account] Logout Success',
    LogoutFailure = '[Account] Logout Failure',
    GetProfile = '[Account] Get Profile',
    GetProfileSuccess = '[Account] Get Profile Success',
    GetProfileFailure = '[Account] Get Profile Failure',
    UpdateProfile = '[Account] Update Profile',
    UpdateProfileSuccess = '[Account] Update Profile Success',
    UpdateProfileFailure = '[Account] Update Profile Failure',

    ResetIsPasswordRecovered = '[Account] Reset isPasswordRecovered',
    ResetIsPasswordRecoveredSuccess = '[Account] Reset isPasswordRecovered Success',
}

export class Login implements Action {
    public readonly type = EAccountActions.Login;
    constructor(public payload: LoginModel) {}
}

export class LoginSuccess implements Action {
    public readonly type = EAccountActions.LoginSuccess;
    constructor(public payload: IAccount) {}
}

export class LoginFailure implements Action {
    public readonly type = EAccountActions.LoginFailure;
    constructor() {}
}

export class CreateAccount implements Action {
    public readonly type = EAccountActions.CreateAccount;
    constructor(public payload: CreateAccountModel) {}
}

export class CreateAccountSuccess implements Action {
    public readonly type = EAccountActions.CreateAccountSuccess;
    constructor() {}
}

export class CreateAccountFailure implements Action {
    public readonly type = EAccountActions.CreateAccountFailure;
    constructor() {}
}

export class LoginByReference implements Action {
    public readonly type = EAccountActions.LoginByReference;
    constructor(public payload: LoginResultModel) {}
}

export class LoginByReferenceSuccess implements Action {
    public readonly type = EAccountActions.LoginByReferenceSuccess;
    constructor(public payload: IAccount) {}
}

export class RecoverPassword implements Action {
    public readonly type = EAccountActions.RecoverPassword;
    constructor(public payload: RecoverPasswordModel) {}
}

export class RecoverPasswordSuccess implements Action {
    public readonly type = EAccountActions.RecoverPasswordSuccess;
    constructor() {}
}

export class RecoverPasswordFailure implements Action {
    public readonly type = EAccountActions.RecoverPasswordFailure;
    constructor() {}
}

export class Logout implements Action {
    public readonly type = EAccountActions.Logout;
    constructor() {}
}

export class LogoutSuccess implements Action {
    public readonly type = EAccountActions.LogoutSuccess;
    constructor() {}
}

export class LogoutFailure implements Action {
    public readonly type = EAccountActions.LogoutFailure;
    constructor() {}
}

export class ResetIsPasswordRecovered implements Action {
    public readonly type = EAccountActions.ResetIsPasswordRecovered;
    constructor() {}
}

export class ResetIsPasswordRecoveredSuccess implements Action {
    public readonly type = EAccountActions.ResetIsPasswordRecoveredSuccess;
    constructor() {}
}

export class GetProfile implements Action {
    public readonly type = EAccountActions.GetProfile;
    constructor() {}
}

export class GetProfileSuccess implements Action {
    public readonly type = EAccountActions.GetProfileSuccess;
    constructor(public payload: ProfileModel) {}
}

export class GetProfileFailure implements Action {
    public readonly type = EAccountActions.GetProfileFailure;
    constructor() {}
}

export class UpdateProfile implements Action {
    public readonly type = EAccountActions.UpdateProfile;
    constructor(public payload: UpdateProfileModel) {}
}

export class UpdateProfileSuccess implements Action {
    public readonly type = EAccountActions.UpdateProfileSuccess;
    constructor() {}
}

export class UpdateProfileFailure implements Action {
    public readonly type = EAccountActions.UpdateProfileFailure;
    constructor() {}
}

export type AccountActions = Login | LoginSuccess | LoginFailure |
                             CreateAccount | CreateAccountSuccess | CreateAccountFailure | 
                             LoginByReference | LoginByReferenceSuccess |
                             Logout | LogoutSuccess | LogoutFailure |
                             RecoverPassword | RecoverPasswordSuccess | RecoverPasswordFailure |
                             GetProfile | GetProfileSuccess | GetProfileFailure |
                             UpdateProfile | UpdateProfileSuccess | UpdateProfileFailure |
                             ResetIsPasswordRecovered | ResetIsPasswordRecoveredSuccess;