import {Action} from '@ngrx/store';
import { OrdersModel } from '../../models/api/orders/orders-model';

export enum EOrdersActions {
    GetOrders = '[Orders] Get Orders',
    GetOrdersSuccess = '[Orders] Get Orders Success',
    GetOrdersFailure = '[Orders] Get Orders Failure',
    Checkout = '[Order] Checkout',
    CheckoutSuccess = '[Order] Checkout Success',
    CheckoutFailure = '[Order] Checkout Failure',
}

export class GetOrders implements Action {
    public readonly type = EOrdersActions.GetOrders;
    constructor() {}
}

export class GetOrdersSuccess implements Action {
    public readonly type = EOrdersActions.GetOrdersSuccess;
    constructor(public payload: OrdersModel) {}
}

export class GetOrdersFailure implements Action {
    public readonly type = EOrdersActions.GetOrdersFailure;
    constructor() {}
}

export class Checkout implements Action {
    public readonly type = EOrdersActions.Checkout;
    constructor(public payload: number) {}
}

export class CheckoutSuccess implements Action {
    public readonly type = EOrdersActions.CheckoutSuccess;
    constructor() {}
}

export class CheckoutFailure implements Action {
    public readonly type = EOrdersActions.CheckoutFailure;
    constructor() {}
}

export type OrdersActions = GetOrders | GetOrdersSuccess | GetOrdersFailure;