import { Action } from '@ngrx/store';
import { GetAuthorsModel } from '../../models/api/author-managment/get-authors-model';
import { AuthorsModel } from '../../models/api/author-managment/authors-model';
import { AddAuthorModel } from '../../models/api/author-managment/add-author-model';
import { UpdateAuthorModel } from '../../models/api/author-managment/update-author-model';

export enum EAuthorManagmentActions {
    GetAuthors = '[AuthorManagment] Get Authors',
    GetAuthorsSuccess = '[AuthorManagment] Get Authors Success',
    GetAuthorsFailure = '[AuthorManagment] Get Authors Failure',
    RemoveAuthor = '[AuthorManagment] Remove Author',
    RemoveAuthorSuccess = '[AuthorManagment] Remove Author Success',
    RemoveAuthorFailure = '[AuthorManagment] Remove Author Failure',
    AddAuthor = '[AuthorManagment] Add Author',
    AddAuthorSuccess = '[AuthorManagment] Add Author Success',
    AddAuthorFailure = '[AuthorManagment] Add Author Failure',
    UpdateAuthor = '[AuthorManagment] Update Author',
    UpdateAuthorSuccess = '[AuthorManagment] Update Author Success',
    UpdateAuthorFailure = '[AuthorManagment] Update Author Failure'
}

export class GetAuthors implements Action {
    public readonly type = EAuthorManagmentActions.GetAuthors;
    constructor(public payload: GetAuthorsModel) { }
}

export class GetAuthorsSuccess implements Action {
    public readonly type = EAuthorManagmentActions.GetAuthorsSuccess;
    constructor(public payload: { data: AuthorsModel, query: GetAuthorsModel }) { }
}

export class GetAuthorsFailure implements Action {
    public readonly type = EAuthorManagmentActions.GetAuthorsFailure;
    constructor(public payload: GetAuthorsModel) { }
}

export class RemoveAuthor implements Action {
    public readonly type = EAuthorManagmentActions.RemoveAuthor;
    constructor(public payload: number) { }
}

export class RemoveAuthorSuccess implements Action {
    public readonly type = EAuthorManagmentActions.RemoveAuthorSuccess;
    constructor() { }
}

export class RemoveAuthorFailure implements Action {
    public readonly type = EAuthorManagmentActions.RemoveAuthorFailure;
    constructor() { }
}

export class AddAuthor implements Action {
    public readonly type = EAuthorManagmentActions.AddAuthor;
    constructor(public payload: AddAuthorModel) { }
}

export class AddAuthorSuccess implements Action {
    public readonly type = EAuthorManagmentActions.AddAuthorSuccess;
    constructor() { }
}

export class AddAuthorFailure implements Action {
    public readonly type = EAuthorManagmentActions.AddAuthorFailure;
    constructor() { }
}

export class UpdateAuthor implements Action {
    public readonly type = EAuthorManagmentActions.UpdateAuthor;
    constructor(public payload: UpdateAuthorModel) { }
}

export class UpdateAuthorSuccess implements Action {
    public readonly type = EAuthorManagmentActions.UpdateAuthorSuccess;
    constructor() { }
}

export class UpdateAuthorFailure implements Action {
    public readonly type = EAuthorManagmentActions.UpdateAuthorFailure;
    constructor() { }
}

export type AuthorManagmentActions = GetAuthors | GetAuthorsSuccess | GetAuthorsFailure
    | RemoveAuthor | RemoveAuthorSuccess | RemoveAuthorFailure
    | AddAuthor | AddAuthorSuccess | AddAuthorFailure
    | UpdateAuthor | UpdateAuthorSuccess | UpdateAuthorFailure;