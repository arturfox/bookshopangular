import {Action} from '@ngrx/store';
import { UsersModel } from '../../models/api/user-managment/users-model';
import { GetUsersModel } from '../../models/api/user-managment/get-users-model';
import { UpdateUserModel } from '../../models/api/user-managment/update-user-model';
//import { BatchAction } from 'ngrx-batch-action-reducer';

export enum EUserManagmentActions {
    GetUsers = '[UserManagment] Get Users',
    GetUsersSuccess = '[UserManagment] Get Users Success',
    GetUsersFailure = '[UserManagment] Get Users Failure',
    SuspendUserAccount = '[UserManagment] Suspend User Account',
    SuspendUserAccountSuccess = '[UserManagment] Suspend User Account Success',
    SuspendUserAccountFailure = '[UserManagment] Suspend User Account Failure',
    ActivateUserAccount = '[UserManagment] Activate User Account',
    ActivateUserAccountSuccess = '[UserManagment] Activate User Account Success',
    ActivateUserAccountFailure = '[UserManagment] Activate User Account Failure',
    RemoveUser = '[UserManagment] Remove User',
    RemoveUserSuccess = '[UserManagment] Remove User Success',
    RemoveUserFailure = '[UserManagment] Remove User Failure',
    UpdateUser = '[UserManagment] Update User',
    UpdateUserSuccess = '[UserManagment] Update User Success',
    UpdateUserFailure = '[UserManagment] Update User Failure',
    //batch actions
    //SuspendUserAccountSuccessGetUsers = '[UserManagment] SuspendUserAccountSuccess & GetUsers'
}

export class GetUsers implements Action {
    public readonly type = EUserManagmentActions.GetUsers;
    constructor(public payload: GetUsersModel) {}
}

export class GetUsersSuccess implements Action {
    public readonly type = EUserManagmentActions.GetUsersSuccess;
    constructor(public payload: { data: UsersModel, query: GetUsersModel }) {}
}

export class GetUsersFailure implements Action {
    public readonly type = EUserManagmentActions.GetUsersFailure;
    constructor(public payload: GetUsersModel) {}
}

export class SuspendUserAccount implements Action {
    public readonly type = EUserManagmentActions.SuspendUserAccount;
    constructor(public payload: number) {}
}

export class SuspendUserAccountSuccess implements Action {
    public readonly type = EUserManagmentActions.SuspendUserAccountSuccess;
    constructor() {}
}

export class SuspendUserAccountFailure implements Action {
    public readonly type = EUserManagmentActions.SuspendUserAccountFailure;
    constructor() {}
}

export class ActivateUserAccount implements Action {
    public readonly type = EUserManagmentActions.ActivateUserAccount;
    constructor(public payload: number) {}
}

export class ActivateUserAccountSuccess implements Action {
    public readonly type = EUserManagmentActions.ActivateUserAccountSuccess;
    constructor() {}
}

export class ActivateUserAccountFailure implements Action {
    public readonly type = EUserManagmentActions.ActivateUserAccountFailure;
    constructor() {}
}

export class RemoveUser implements Action {
    public readonly type = EUserManagmentActions.RemoveUser;
    constructor(public payload: number) {}
}

export class RemoveUserSuccess implements Action {
    public readonly type = EUserManagmentActions.RemoveUserSuccess;
    constructor() {}
}

export class RemoveUserFailure implements Action {
    public readonly type = EUserManagmentActions.RemoveUserFailure;
    constructor() {}
}

export class UpdateUser implements Action {
    public readonly type = EUserManagmentActions.UpdateUser;
    constructor(public payload: UpdateUserModel) {}
}

export class UpdateUserSuccess implements Action {
    public readonly type = EUserManagmentActions.UpdateUserSuccess;
    constructor() {}
}

export class UpdateUserFailure implements Action {
    public readonly type = EUserManagmentActions.UpdateUserFailure;
    constructor() {}
}

/*@BatchAction() 
export class SuspendUserAccountSuccessGetUsers implements Action {
    public readonly type = EUserManagmentActions.SuspendUserAccountSuccessGetUsers;
    constructor(public payload: [
        SuspendUserAccountSuccess, 
        GetUsers
    ]) {}
}*/

export type UserManagmentActions = GetUsers | GetUsersSuccess | GetUsersFailure
                                 | SuspendUserAccount | SuspendUserAccountSuccess | SuspendUserAccountFailure
                                 | ActivateUserAccount | ActivateUserAccountFailure | ActivateUserAccountFailure
                                 | RemoveUser | RemoveUserSuccess | RemoveUserFailure
                                 | UpdateUser | UpdateUserSuccess | UpdateUserFailure;
                                // | SuspendUserAccountSuccessGetUsers;