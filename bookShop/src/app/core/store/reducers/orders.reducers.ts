import { initialOrdersState, IOrdersState } from '../state/orders.state';
import { OrdersActions, EOrdersActions } from '../actions/orders.actions';
import { Orders } from '../../models/orders';

export const ordersReducers = (
    state = initialOrdersState,
    action: OrdersActions
): IOrdersState => {
    switch (action.type) {
        case EOrdersActions.GetOrdersSuccess: {

            let data = new Orders();
            data.orders = action.payload.orders;

            return {
                ...state,
                orders: data
            };
        }       
        default:
            return state;

    }
}