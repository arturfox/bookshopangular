import { initialAuthorManagmentState, IAuthorManagmentState } from '../state/author-managment.state';
import { AuthorManagmentActions, EAuthorManagmentActions } from '../actions/author-managment.actions';

export const authorManagmentRedusers = (
    state = initialAuthorManagmentState,
    action: AuthorManagmentActions
): IAuthorManagmentState => {
    switch (action.type) {
        case EAuthorManagmentActions.GetAuthorsSuccess: {
            return {
                ...state,
                authors: action.payload.data,
                lastExecutedQuery: action.payload.query
            };
        }
        case EAuthorManagmentActions.GetAuthorsFailure: {
            return {
                ...state,
                lastExecutedQuery: action.payload
            };
        }
        
        default:
            return state;
    }
}