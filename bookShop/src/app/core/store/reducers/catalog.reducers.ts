import { initialCatalogState, ICatalogState } from '../state/catalog.state';
import { CatalogActions, ECatalogActions } from '../actions/catalog.actions';
import { CatalogDetails } from '../../models/catalog-details';

export const catalogReducers = (
    state = initialCatalogState,
    action: CatalogActions
): ICatalogState => {
    switch (action.type) {
        case ECatalogActions.GetCatalogSuccess: {
            return {
                ...state,
                catalog: action.payload
            };
        }
        case ECatalogActions.GetCatalogDetailsSuccess: {

            let details = new CatalogDetails();
            details.data = action.payload;

            return {
                ...state,
                details: details
            };
        }
        
        default:
            return state;

    }
}