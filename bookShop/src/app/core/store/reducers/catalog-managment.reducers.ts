import { initialCatalogManagmentState, ICatalogManagmentState } from '../state/catalog-managment-state';
import { CatalogManagmentActions, ECatalogManagmentActions } from '../actions/catalog-managment.actions';

export const catalogManagmentRedusers = (
    state = initialCatalogManagmentState,
    action: CatalogManagmentActions
): ICatalogManagmentState => {
    switch (action.type) {
        case ECatalogManagmentActions.GetCatalogSuccess: {
            return {
                ...state,
                items: action.payload.data,
                lastExecutedQuery: action.payload.query
            };
        }
        case ECatalogManagmentActions.GetCatalogFailure: {
            return {
                ...state,
                lastExecutedQuery: action.payload
            };
        }
        case ECatalogManagmentActions.GetAllAuthorsSuccess: {
            return {
                ...state,
                allAuthors: action.payload.items
            };
        }
        
        default:
            return state;
    }
}