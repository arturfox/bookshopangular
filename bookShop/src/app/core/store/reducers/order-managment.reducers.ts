import { IOrderManagmentState, initialOrderManagmentState } from '../state/order-managment.state';
import { OrderManagmentActions, EOrderManagmentActions } from '../actions/order-managment.actions';

export const orderManagmentRedusers = (
    state = initialOrderManagmentState,
    action: OrderManagmentActions
): IOrderManagmentState => {
    switch (action.type) {
        case EOrderManagmentActions.GetOrdersSuccess: {
            return {
                ...state,
                orders: action.payload
            };
        }
        
        default:
            return state;
    }
}