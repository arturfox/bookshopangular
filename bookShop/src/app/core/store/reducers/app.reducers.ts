import { ActionReducerMap } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { accountReducers } from './account.reducers';
import { routerReducer } from '@ngrx/router-store';
import { catalogReducers } from './catalog.reducers';
import { cartReducers } from './cart.reducers';
import { ordersReducers } from './orders.reducers';
import { userManagmentRedusers } from './user-managment.reducers';
import { orderManagmentRedusers } from './order-managment.reducers';
import { authorManagmentRedusers } from './author-managment.reducers';
import { catalogManagmentRedusers } from './catalog-managment.reducers';

export const appReducers: ActionReducerMap<IAppState, any> = {
    router: routerReducer,
    account: accountReducers,
    catalog: catalogReducers,
    cart: cartReducers,
    orders: ordersReducers,
    userManagment: userManagmentRedusers,
    orderManagment: orderManagmentRedusers,
    authorManagment: authorManagmentRedusers,
    catalogManagment: catalogManagmentRedusers
};