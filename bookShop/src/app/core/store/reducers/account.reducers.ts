import { initialAccountState, IAccountState } from '../state/account.state';
import { EAccountActions, AccountActions } from '../actions/account.actions';

export const accountReducers = (
    state = initialAccountState,
    action: AccountActions
): IAccountState => {
    switch (action.type) {
        case EAccountActions.LoginSuccess: {
            return {
                ...state,
                currentUser: action.payload
            };
        }
        case EAccountActions.LoginByReferenceSuccess: {
            return {
                ...state,
                currentUser: action.payload
            };
        }
        case EAccountActions.RecoverPasswordSuccess: {
            return {
                ...state,
                isPasswordRecovered: true
            };
        }
        case EAccountActions.ResetIsPasswordRecoveredSuccess: {
            return {
                ...state,
                isPasswordRecovered: false
            };
        }
        case EAccountActions.LogoutFailure: {
            return {
                ...state,
                isPasswordRecovered: false,
                currentUser: null
            };
        }
        case EAccountActions.LogoutSuccess: {
            return {
                ...state,
                isPasswordRecovered: false,
                currentUser: null
            };
        }
        case EAccountActions.GetProfileSuccess: {
            return {
                ...state,
                isPasswordRecovered: false,
                profile: action.payload
            };
        }

        default:
            return state;

    }
}