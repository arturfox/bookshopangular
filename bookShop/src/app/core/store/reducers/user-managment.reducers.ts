import { initialUserManagmentState, IUserManagmentState } from '../state/user-managment.state';
import { UserManagmentActions, EUserManagmentActions } from '../actions/user-managment.actions';

export const userManagmentRedusers = (
    state = initialUserManagmentState,
    action: UserManagmentActions
): IUserManagmentState => {
    switch (action.type) {
        case EUserManagmentActions.GetUsersSuccess: {
            return {
                ...state,
                users: action.payload.data,
                lastExecutedQuery: action.payload.query
            };
        }
        case EUserManagmentActions.GetUsersFailure: {
            return {
                ...state,
                lastExecutedQuery: action.payload
            };
        }
        
        default:
            return state;
    }
}