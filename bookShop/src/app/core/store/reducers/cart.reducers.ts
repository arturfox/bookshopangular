import { CartActions, ECartActions } from '../actions/cart.actions';
import { initialCartState, ICartState } from '../state/cart.state';

export const cartReducers = (
    state = initialCartState,
    action: CartActions
): ICartState => {
    switch (action.type) { 
        case ECartActions.GetCartSuccess: {
            return {
                ...state,
                cart: action.payload
            };
        }       
        default:
            return state;
    }
}