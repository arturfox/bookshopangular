import { ICart } from '../../models/cart.interface';

export interface ICartState{
    cart: ICart;
}

export const initialCartState: ICartState = {
    cart: null
}