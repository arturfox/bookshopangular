import { IUserManagment } from '../../models/user-managment.interface';
import { GetUsersModel } from '../../models/api/user-managment/get-users-model';

export interface IUserManagmentState{
    users: IUserManagment;
    lastExecutedQuery: GetUsersModel;
}

export const initialUserManagmentState: IUserManagmentState = {
    users: null,
    lastExecutedQuery: null
}