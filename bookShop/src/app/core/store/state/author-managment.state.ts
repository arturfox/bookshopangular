import { GetAuthorsModel } from '../../models/api/author-managment/get-authors-model';
import { IAuthorManagment } from '../../models/author-managment.interface';

export interface IAuthorManagmentState {
    authors: IAuthorManagment;
    lastExecutedQuery: GetAuthorsModel;
}

export const initialAuthorManagmentState: IAuthorManagmentState = {
    authors: null,
    lastExecutedQuery: null
}