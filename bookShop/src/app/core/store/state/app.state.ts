import { IAccountState, initialAccountState } from './account.state';
import { RouterReducerState } from "@ngrx/router-store";
import { ICatalogState, initialCatalogState } from './catalog.state';
import { ICartState, initialCartState } from './cart.state';
import { IOrdersState, initialOrdersState } from './orders.state';
import { initialUserManagmentState, IUserManagmentState } from './user-managment.state';
import { IOrderManagmentState, initialOrderManagmentState } from './order-managment.state';
import { IAuthorManagmentState, initialAuthorManagmentState } from './author-managment.state';
import { ICatalogManagmentState, initialCatalogManagmentState } from './catalog-managment-state';

export interface IAppState {
    router?: RouterReducerState,
    account: IAccountState;
    catalog: ICatalogState;
    cart: ICartState;
    orders: IOrdersState;
    userManagment: IUserManagmentState;
    orderManagment: IOrderManagmentState;
    authorManagment: IAuthorManagmentState;
    catalogManagment: ICatalogManagmentState;
}

export const initialAppState: IAppState = {
    account: initialAccountState,
    catalog: initialCatalogState,
    cart: initialCartState,
    orders: initialOrdersState,
    userManagment: initialUserManagmentState,
    orderManagment: initialOrderManagmentState,
    authorManagment: initialAuthorManagmentState,
    catalogManagment: initialCatalogManagmentState
}

export function getInitialState(): IAppState {
    return initialAppState;
}