import { ICatalogManagment } from '../../models/catalog-managment.interface';
import { GetCatalogModel } from '../../models/api/catalog-managment/get-catalog-model';
import { AllAuthorsModelItem } from '../../models/api/author-managment/all-authors-model-items';

export interface ICatalogManagmentState{
    items: ICatalogManagment;
    lastExecutedQuery: GetCatalogModel;
    allAuthors: AllAuthorsModelItem[];
}

export const initialCatalogManagmentState: ICatalogManagmentState = {
    items: null,
    lastExecutedQuery: null,
    allAuthors: null
}