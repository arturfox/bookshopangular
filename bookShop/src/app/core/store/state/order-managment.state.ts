import { IOrderManagment } from '../../models/order-managment.interface';

export interface IOrderManagmentState{
    orders: IOrderManagment;
}

export const initialOrderManagmentState: IOrderManagmentState = {
    orders: null
}