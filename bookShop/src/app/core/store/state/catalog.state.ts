import { ICatalog } from '../../models/catalog.interface';
import { ICatalogDetails } from '../../models/catalog-details.interface';

export interface ICatalogState{
    catalog: ICatalog;
    details: ICatalogDetails
}

export const initialCatalogState: ICatalogState = {
    catalog: null,
    details: null
}