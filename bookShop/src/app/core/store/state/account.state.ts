import { IAccount } from '../../models/account.interface';
import { ProfileModel } from '../../models/api/authentication/profile-model';

export interface IAccountState{
    currentUser: IAccount;
    profile: ProfileModel;
    isPasswordRecovered: boolean;
}

export const initialAccountState: IAccountState = {
    currentUser: null,
    profile: null,
    isPasswordRecovered: false
}