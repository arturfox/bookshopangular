import { IOrders } from '../../models/orders.interface';

export interface IOrdersState{
    orders: IOrders;
}

export const initialOrdersState: IOrdersState = {
    orders: null
}