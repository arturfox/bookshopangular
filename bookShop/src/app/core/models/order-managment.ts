import { OrdersModelItem } from './api/order-managment/orders-model-item';
import { IOrderManagment } from './order-managment.interface';

export class OrderManagment implements IOrderManagment {
    orders: OrdersModelItem[];
    itemsPerPage: number;
    currentPage: number;
    totalItems: number;
}