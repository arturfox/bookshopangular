import { CatalogModelItem } from './api/catalog/catalog-model-item';

export interface ICatalog {
    items: CatalogModelItem[];  
    itemsPerPage: number;
    currentPage: number;
    totalItems: number;
}