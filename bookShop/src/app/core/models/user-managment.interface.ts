import { UsersModelItem } from './api/user-managment/users-model-item';

export interface IUserManagment {
    users: UsersModelItem[];
    itemsPerPage: number;
    currentPage: number;
    totalItems: number;
}