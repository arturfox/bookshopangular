import { OrdersModelItem } from './api/order-managment/orders-model-item';

export interface IOrderManagment {
    orders: OrdersModelItem[];
    itemsPerPage: number;
    currentPage: number;
    totalItems: number;
}