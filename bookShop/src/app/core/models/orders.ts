import { IOrders } from './orders.interface';
import { OrdersModelItem } from './api/orders/orders-model-item';

export class Orders implements IOrders {
    orders: OrdersModelItem[];
}