import { OrdersModelItem } from './api/orders/orders-model-item';

export interface IOrders {
    orders: OrdersModelItem[];
}