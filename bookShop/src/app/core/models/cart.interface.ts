import { CartModelItem } from './api/cart/cart-model-item';
import { Currency } from '../enums/currency.enum';

export interface ICart {
    total: number;
    currency: Currency;
    items: CartModelItem[];
}