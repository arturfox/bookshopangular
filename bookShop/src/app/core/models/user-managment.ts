import { UsersModelItem } from './api/user-managment/users-model-item';
import { IUserManagment } from './user-managment.interface';

export class UserManagment implements IUserManagment {
    users: UsersModelItem[];
    itemsPerPage: number;
    currentPage: number;
    totalItems: number;
}