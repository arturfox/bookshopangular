import { ICart } from './cart.interface';
import { Currency } from '../enums/currency.enum';
import { CartModelItem } from './api/cart/cart-model-item';

export class Cart implements ICart {
    total: number;
    currency: Currency;
    items: CartModelItem[];
}