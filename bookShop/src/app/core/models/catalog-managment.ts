import { CatalogModelItem } from './api/catalog-managment/catalog-model-item';
import { ICatalogManagment } from './catalog-managment.interface';

export class CatalogManagment implements ICatalogManagment {
    items: CatalogModelItem[];
    itemsPerPage: number;
    currentPage: number;
    totalItems: number;
}