import { AuthorsModelItem } from './api/author-managment/authors-model-item';

export interface IAuthorManagment {
    items: AuthorsModelItem[];
    itemsPerPage: number;
    currentPage: number;
    totalItems: number;
}