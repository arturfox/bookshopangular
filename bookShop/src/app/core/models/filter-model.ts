import { PrintingEditionType } from '../enums/printing-edition-type.enum';

export class FilterModel {
    categories: PrintingEditionType[];
    minValue: number;
    maxValue: number;
}