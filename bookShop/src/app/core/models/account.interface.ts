import { UserRole } from '../enums/user-role.enum';

export interface IAccount {
    firstName: string;
    lastName: string;
    userRole: UserRole;
    token: string;
    refreshToken: string;
    isFirstLogin: boolean;
}