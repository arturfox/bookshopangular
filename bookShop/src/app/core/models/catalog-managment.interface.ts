import { CatalogModelItem } from './api/catalog-managment/catalog-model-item';

export interface ICatalogManagment {
    items: CatalogModelItem[];
    itemsPerPage: number;
    currentPage: number;
    totalItems: number;
}