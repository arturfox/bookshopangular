import { IAccount } from './account.interface';
import { UserRole } from '../enums/user-role.enum';

export class Account implements IAccount {
    firstName: string;
    lastName: string;
    userRole: UserRole;
    token: string;
    refreshToken: string;
    isFirstLogin: boolean;
}