import { ICatalog } from './catalog.interface';
import { CatalogModelItem } from './api/catalog/catalog-model-item';

export class Catalog implements ICatalog {
    items: CatalogModelItem[];
    itemsPerPage: number;
    currentPage: number;
    totalItems: number;
}