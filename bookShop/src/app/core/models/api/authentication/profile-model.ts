export class ProfileModel{
    firstName: string;
    lastName: string;
    userName: string;
    email: string;
    imageSource: string;
}