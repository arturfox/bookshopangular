export class LoginResultModel {

    public token: string;
    public refreshToken: string; 
}