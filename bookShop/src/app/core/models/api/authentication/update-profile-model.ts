export class UpdateProfileModel
{
    firstName: string;
    lastName: string;
    userName: string;
    email: string;
    imageSource: string;
    password: string;
    confirmPassword: string;
}