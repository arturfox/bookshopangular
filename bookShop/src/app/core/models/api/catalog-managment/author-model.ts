export class AuthorModel{
    id: number;
    firstName: string;
    lastName: string;
}