import { Currency } from '../../../enums/currency.enum';
import { PrintingEditionType } from '../../../enums/printing-edition-type.enum';

export class UpdateCatalogItemModel
{
    id: number;
    title: string;
    description: string;
    printingEditionType: PrintingEditionType;
    price: number;
    currency: Currency;
    imageSource: string;
    authorsIds: number[];
}