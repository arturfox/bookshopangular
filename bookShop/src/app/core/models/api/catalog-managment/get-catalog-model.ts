import { CatalogManagmentSortType } from '../../../enums/catalog-managment-sort-type.enum';
import { PrintingEditionType } from '../../../enums/printing-edition-type.enum';

export class GetCatalogModel {
    page: number;
    takeItems: number;
    sortBy: CatalogManagmentSortType;
    types: PrintingEditionType[];
}