import { AccountStatus } from '../../../enums/account-status.enum';

export class UsersModelItem
{
    id: number;
    firstName: string;
    lastName: string;
    userName: string;
    email: string;
    accountStatus: AccountStatus;
}