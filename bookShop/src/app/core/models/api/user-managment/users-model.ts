import { UsersModelItem } from './users-model-item';

export class UsersModel {
    users: UsersModelItem[];
    itemsPerPage: number;
    currentPage: number;
    totalItems: number;
}