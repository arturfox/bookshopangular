import { AccountStatus } from '../../../enums/account-status.enum';
import { UserManagmentSortType } from '../../../enums/user-managment-sort-type.enum';

export class GetUsersModel {
    page: number;
    takeItems: number;
    sortBy: UserManagmentSortType;
    accountStates: AccountStatus[];
    searchQuery: string;
}