export class UpdateUserModel{
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    userName: string;
    
    password: string;
    confirmPassword: string;
}