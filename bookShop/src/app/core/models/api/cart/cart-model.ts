import { Currency } from '../../../enums/currency.enum';
import { CartModelItem } from './cart-model-item';

export class CartModel{
    total: number;
    currency: Currency;
    items: CartModelItem[];
}