import { Currency } from '../../../enums/currency.enum';

export class CartModelItem {
    quantity: number;
    price: number;
    total: number;
    currency: Currency;
    title: string;
    printingEditionId: number;
    cartItemId: number;
}