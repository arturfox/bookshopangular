export class AddToCartModel {
    public positionId: number;
    public quantity: number;
}