import { SortType } from '../../../enums/sort-type.enum';
import { PrintingEditionType } from '../../../enums/printing-edition-type.enum';

export class GetCatalogModel {
    page: number;
    takeItems: number;
    orderBy: SortType;
    categories: PrintingEditionType[];
    minValue: number;
    maxValue: number;
    searchQuery: string;
}