import { Currency } from '../../../enums/currency.enum';
import { AuthorModel } from './author-model';
import { PrintingEditionType } from '../../../enums/printing-edition-type.enum';

export class CatalogDetailsModel {
    id: number;
    title: string;
    description: string;
    price: number;
    currency: Currency;
    imageSource: string;
    authors: AuthorModel[];
    printingEditionType: PrintingEditionType;
}