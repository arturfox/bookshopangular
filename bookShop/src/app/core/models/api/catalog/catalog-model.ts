import { CatalogModelItem } from './catalog-model-item';

export class CatalogModel {
    items: CatalogModelItem[];
    itemsPerPage: number;
    currentPage: number;
    totalItems: number;
}