import { OrderManagmentSortType } from '../../../enums/order-managment-sort-type.enum';
import { OrderStatus } from '../../../enums/order-status.enum';

export class GetOrdersModel {
    page: number;
    takeItems: number;
    sortBy: OrderManagmentSortType;
    orderStates: OrderStatus[];
}