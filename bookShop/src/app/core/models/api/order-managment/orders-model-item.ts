import { Currency } from '../../../enums/currency.enum';
import { OrderStatus } from '../../../enums/order-status.enum';
import { OrderPositionModel } from './order-position-model';

export class OrdersModelItem
{
    orderId: number;
    orderTime: Date;
    userName: string;
    email: string;
    orderAmount: number;
    currency: Currency;
    orderStatus: OrderStatus;
    positions: OrderPositionModel[];
}