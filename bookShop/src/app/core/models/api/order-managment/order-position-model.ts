import { PrintingEditionType } from '../../../enums/printing-edition-type.enum';

export class OrderPositionModel{
    printingEditionType: PrintingEditionType;
    title: string;
    quantity: number;
}