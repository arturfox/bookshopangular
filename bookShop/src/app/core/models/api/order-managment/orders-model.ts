import { OrdersModelItem } from './orders-model-item';

export class OrdersModel {
    orders: OrdersModelItem[];
    itemsPerPage: number;
    currentPage: number;
    totalItems: number;
}