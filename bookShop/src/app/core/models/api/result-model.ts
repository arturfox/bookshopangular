export class ResultModel<T = void>{
    
    public data: T;
    public statusCode: number;
    public isSucceed: boolean;
    public errors: Array<string>;
    public defaultErrorMessage: string;
}