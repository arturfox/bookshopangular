import { AllAuthorsModelItem } from './all-authors-model-items';

export class AllAuthorsModel {
    items: AllAuthorsModelItem[];
}