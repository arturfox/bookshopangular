export class AllAuthorsModelItem{
    id: number;
    firstName: string;
    lastName: string;
}