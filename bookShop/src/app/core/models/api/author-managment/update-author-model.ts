
export class UpdateAuthorModel {
    id: number;
    firstName: string;
    lastName: string;
}