import { AuthorsModelItem } from './authors-model-item';

export class AuthorsModel {
    items: AuthorsModelItem[];
    itemsPerPage: number;
    currentPage: number;
    totalItems: number;
}