import { AuthorManagmentSortType } from '../../../enums/author-managment-sort-type.enum';

export class GetAuthorsModel {
    page: number;
    takeItems: number;
    sortBy: AuthorManagmentSortType;
}