import { PrintingEditionModel } from './printing-edition-model';

export class AuthorsModelItem{
    id: number;
    firstName: string;
    lastName: string;
    printingEditions: PrintingEditionModel[];
}