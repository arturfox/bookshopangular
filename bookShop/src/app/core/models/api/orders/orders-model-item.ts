import { OrderStatus } from '../../../enums/order-status.enum';
import { Currency } from '../../../enums/currency.enum';
import { OrderPositionModel } from './order-position-model';

export class OrdersModelItem {
    orderId: number;
    orderTime: Date;
    orderStatus: OrderStatus;
    currency: Currency;
    total: number;
    positions: OrderPositionModel[];
}