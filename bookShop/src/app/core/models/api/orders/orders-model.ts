import { OrdersModelItem } from './orders-model-item';

export class OrdersModel {
    orders: OrdersModelItem[];
}