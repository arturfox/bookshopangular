import { AuthorsModelItem } from './api/author-managment/authors-model-item';
import { IAuthorManagment } from './author-managment.interface';

export class AuthorManagment implements IAuthorManagment {
    items: AuthorsModelItem[];
    itemsPerPage: number;
    currentPage: number;
    totalItems: number;
}