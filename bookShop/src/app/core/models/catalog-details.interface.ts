import { CatalogDetailsModel } from './api/catalog/catalog-details-model';

export interface ICatalogDetails {
    data: CatalogDetailsModel;
}