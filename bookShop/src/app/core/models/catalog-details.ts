import { CatalogDetailsModel } from './api/catalog/catalog-details-model';
import { ICatalogDetails } from './catalog-details.interface';

export class CatalogDetails implements ICatalogDetails {
    data: CatalogDetailsModel;
}