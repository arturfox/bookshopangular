import { Injectable } from "@angular/core";
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from "@angular/common/http";
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { switchMap, filter, take, catchError, withLatestFrom, first } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { IAppState } from '../store/state/app.state';
import { AccountService } from '../services/account.service';
import { Logout, LoginSuccess } from '../store/actions/account.actions';
import { RefreshTokenModel } from '../models/api/authentication/refresh-token-model';
import { LoginResultModel } from '../models/api/authentication/login-result-model';
import { JwtHelper } from '../helpers/jwt.helper';
import { Account } from '../models/account';
import { UserRole } from '../enums/user-role.enum';
import { selectRefreshToken } from '../store/selectors/account.selectors';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {
  private refreshTokenInProgress = false;
  // Refresh Token Subject tracks the current token, or is null if no token is currently
  // available (e.g. refresh pending).
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    null
  );
  constructor(private accountService: AccountService,
    private _store: Store<IAppState>,
    private _jwtHelper: JwtHelper) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(

      catchError(err => {
        // We don't want to refresh token for some requests like login or refresh token itself
        // So we verify url and we throw an error if it's the case

        if (request.url.includes("account/token/refresh") ||
          request.url.includes("account/login") ||
          request.url.includes("account/register") ||
          request.url.includes("assets/") ||
          request.url.includes("account/logout/")) {
          // We do another check to see if refresh token failed
          // In this case we want to logout user and to redirect it to login page

          if (request.url.includes("account/token/refresh")) {

            this.logOut();
          }
          return throwError(err);
        }

        // If error status is different than 401 we want to skip refresh token
        // So we check that and throw the error if it's the case
        if (err.status !== 401) {
          return throwError(err);
        }

        if (this.refreshTokenInProgress) {
          // If refreshTokenInProgress is true, we will wait until refreshTokenSubject has a non-null value
          // – which means the new token is ready and we can retry the request again

          return this.refreshTokenSubject.pipe(
            filter(result => result !== null),
            take(1),
            withLatestFrom(this._store),
            take(1),
            switchMap((withStore) => next.handle(this.addAuthenticationToken(request, withStore["1"].account.currentUser.token))));
        } else {

          this.refreshTokenInProgress = true;

          // Set the refreshTokenSubject to null so that subsequent API calls will wait until the new token has been retrieved
          this.refreshTokenSubject.next(null);

          // Call auth.refreshAccessToken(this is an Observable that will be returned)          
             
              return this._store.select(selectRefreshToken).pipe(
                first(),
                switchMap((withStore) => {

                  let model = new RefreshTokenModel();
                  model.refreshToken = withStore;

                  return this.accountService.RefreshToken(model).pipe(
                    switchMap((refreshResponse) => {
                      
                      if(!refreshResponse.isSucceed){
                        this.refreshTokenInProgress = false;
                        this.logOut();

                        return throwError(err);
                      }
    
                      //When the call to refreshToken completes we reset the refreshTokenInProgress to false
                      // for the next time the token needs to be refreshed
                      this.saveUserInfo(refreshResponse.data);
    
                      this.refreshTokenInProgress = false;
                      this.refreshTokenSubject.next(refreshResponse);
    
                      return next.handle(this.addAuthenticationToken(request, refreshResponse.data.token));
                    }),
                    catchError((err: any) => {
                      this.refreshTokenInProgress = false;
  
                      this.logOut();
   
                      return throwError(err);
                    }))
                })
              );         
        }
      })
    );
  }

  saveUserInfo(responceData: LoginResultModel) {

    let data = this._jwtHelper.getPayloadDataFromJWT(responceData.token);

    let account = new Account();
    account.token = responceData.token;
    account.refreshToken = responceData.token;
    account.firstName = data.given_name;
    account.lastName = data.family_name;
    account.userRole = UserRole[<string>data.role];
    account.isFirstLogin = false;

    this._store.dispatch(new LoginSuccess(account))
  }

  logOut() {

    this._store.dispatch(new Logout());
  }

  addAuthenticationToken(request, authToken: string) {

    if (!authToken) {
      return request;
    }

    return request.clone({
      headers: request.headers.set('Authorization', `Bearer ${authToken}`)
    });
  }
}
