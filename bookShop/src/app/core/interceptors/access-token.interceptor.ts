import { Injectable, } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { IAppState } from '../store/state/app.state';
import { Store } from '@ngrx/store';
import { flatMap, first } from 'rxjs/operators';
import { selectAccessToken } from '../store/selectors/account.selectors';

@Injectable()
export class AcessTokenInterceptor implements HttpInterceptor {
    constructor(private _store: Store<IAppState>) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {

        if (req.url.includes("account/token/refresh") ||
            req.url.includes("account/login") ||
            req.url.includes("account/createaccount/") ||
            req.url.includes("assets/")) {

            return next.handle(req);
        }

        return this._store.select(selectAccessToken).pipe(
            first(),
            flatMap(token => {
                
              const authReq = !!token ? req.clone({
                setHeaders: { Authorization: `Bearer ${token}` },
              }) : req;
              return next.handle(authReq);
            }),
          );
    }
}