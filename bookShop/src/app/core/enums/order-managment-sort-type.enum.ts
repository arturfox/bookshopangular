export enum OrderManagmentSortType {
    None = 0,
    NumberDesc = 1,
    NumberAsc = 2,
    DateDesc = 3,
    DateAsc = 4,
    AmountDesc = 5,
    AmountAsc = 6
}
