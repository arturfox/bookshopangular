export enum UserManagmentSortType {
    None = 0,
    UserNameDesc = 1,
    UserNameAsc = 2,
    EmailDesc = 3,
    EmailAsc = 4
}
