export enum SortType {
    None = 0,
    LowToHigh = 1,
    HighToLow = 2
}