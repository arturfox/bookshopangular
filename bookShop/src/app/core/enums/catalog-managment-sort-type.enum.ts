export enum CatalogManagmentSortType
{
    None = 0,
    NumberDesc = 1,
    NumberAsc = 2,
    NameDesc = 3,
    NameAsc = 4,
    PriceDesc = 5,
    PriceAsc = 6
}