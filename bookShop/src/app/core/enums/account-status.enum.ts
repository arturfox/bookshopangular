export enum AccountStatus {
    None = 0,
    Active = 1,
    Suspended = 2
}
