import { Injectable } from '@angular/core';
import { HttpHelper } from '../helpers/httphelper';
import { ResultModel } from '../models/api/result-model';
import { Observable } from 'rxjs';
import { GetCatalogModel } from '../models/api/catalog-managment/get-catalog-model';
import { CatalogModel } from '../models/api/catalog-managment/catalog-model';
import { AddCatalogItemModel } from '../models/api/catalog-managment/add-catalog-item-model';
import { UpdateCatalogItemModel } from '../models/api/catalog-managment/update-catalog-item-model';

@Injectable({
  providedIn: 'root'
})
export class CatalogManagmentService {

  constructor(private httphelper: HttpHelper) {
  }

  public GetCatalog(model: GetCatalogModel): Observable<ResultModel<CatalogModel>> {

    let params = this.GenerateRoutingParams(model);
    const route = `catalogmanagment/get${params}`;

    return this.httphelper.Get<CatalogModel>(`${route}`);
  }

  public RemoveProduct(id: number): Observable<ResultModel> {

    const route = `catalogmanagment/remove/${id}`;
    return this.httphelper.Post(`${route}`);
  }

  public AddProduct(product: AddCatalogItemModel): Observable<ResultModel> {
    
    const route = `catalogmanagment/add`;
    return this.httphelper.Post(`${route}`, product);
  }

  public UpdateProduct(product: UpdateCatalogItemModel): Observable<ResultModel> {
    
    const route = `catalogmanagment/update`;
    return this.httphelper.Post(`${route}`, product);
  }

  private GenerateRoutingParams(model: GetCatalogModel): string {

    let route = `?takeItems=${model.takeItems}&page=${model.page}&sortType=${model.sortBy}`;

    if (model.types === undefined || model.types.length === 0) {
      return route;
    }

    route = `${route}&productCategories=${model.types[0]}`;

    if (model.types.length === 1) {
      return route;
    }

    for (let i = 1; i < model.types.length; i++) {
      route = `${route},${model.types[i]}`;
    }

    return route;
  }
}
