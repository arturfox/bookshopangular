import { Injectable } from '@angular/core';
import { HttpHelper } from '../helpers/httphelper';
import { ResultModel } from '../models/api/result-model';
import { Observable } from 'rxjs';
import { OrdersModel } from '../models/api/orders/orders-model';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  constructor(private httphelper: HttpHelper) {
  }

  public GetOrders(): Observable<ResultModel<OrdersModel>> {
    const route = `order/get`;
    return this.httphelper.Get<OrdersModel>(`${route}`);
  }
}
