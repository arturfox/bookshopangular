import { Injectable } from '@angular/core';
import { HttpHelper } from '../helpers/httphelper';
import { ResultModel } from '../models/api/result-model';
import { Observable } from 'rxjs';
import { AddToCartModel } from '../models/api/cart/add-to-cart-model';
import { CartModel } from '../models/api/cart/cart-model';
import { RemovePositionModel } from '../models/api/cart/remove-position-model';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private httphelper: HttpHelper) {
  }

  public AddToCart(model: AddToCartModel): Observable<ResultModel> {

    const route = `cart/addPosition`;
    return this.httphelper.Post(route, model);
  }

  public GetCart(): Observable<ResultModel<CartModel>> {

    const route = `cart/getCart`;
    return this.httphelper.Get(route);
  }

  public RemovePosition(data: RemovePositionModel): Observable<ResultModel> {

    const route = `cart/removePosition`;
    return this.httphelper.Post(route, data);
  }

}
