import { Injectable } from '@angular/core';
import { HttpHelper } from '../helpers/httphelper';
import { GetCatalogModel } from '../models/api/catalog/get-catalog-model';
import { ResultModel } from '../models/api/result-model';
import { Observable } from 'rxjs';
import { CatalogModel } from '../models/api/catalog/catalog-model';
import { CatalogDetailsModel } from '../models/api/catalog/catalog-details-model';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  constructor(private httphelper: HttpHelper) {
  }

  public GetCatalog(model: GetCatalogModel): Observable<ResultModel<CatalogModel>> {

    let params = this.GenerateRoutingParams(model);
    const route = `catalog/getList${params}`;

    return this.httphelper.Get<CatalogModel>(`${route}`);
  }

  public GetCatalogDetails(id: number): Observable<ResultModel<CatalogDetailsModel>> {

    const route = `catalog/details?printingEditionId=${id}`;

    return this.httphelper.Get<CatalogDetailsModel>(`${route}`);
  }

  private GenerateRoutingParams(model: GetCatalogModel): string {

    let route = `?takeItems=${model.takeItems}&page=${model.page}&orderBy=${model.orderBy}`;

    if(model.minValue !== undefined){
      route = `${route}&minValue=${model.minValue}`; 
    }

    if(model.maxValue !== undefined){
      route = `${route}&maxValue=${model.maxValue}`; 
    }

    if(model.searchQuery !== undefined && model.searchQuery !== "%" && model.searchQuery !== ""){
      route = `${route}&searchQuery=${model.searchQuery}`; 
    }

    if (model.categories === undefined || model.categories.length === 0) {
      return route;
    }

    route = `${route}&categories=`;

    route = `${route}${model.categories[0]}`;

    if(model.categories.length === 1){
      return route;
    }

    for (let i = 1; i < model.categories.length; i++) {

      route = `${route},${model.categories[i]}`;
    }

    return route;
  }
}
