import { Injectable } from '@angular/core';
import { HttpHelper } from '../helpers/httphelper';
import { ResultModel } from '../models/api/result-model';
import { Observable } from 'rxjs';
import { GetAuthorsModel } from '../models/api/author-managment/get-authors-model';
import { AuthorsModel } from '../models/api/author-managment/authors-model';
import { AddAuthorModel } from '../models/api/author-managment/add-author-model';
import { UpdateAuthorModel } from '../models/api/author-managment/update-author-model';
import { AllAuthorsModel } from '../models/api/author-managment/all-authors-model';

@Injectable({
  providedIn: 'root'
})
export class AuthorManagmentService {

  constructor(private httphelper: HttpHelper) {}

  public GetAuthors(model: GetAuthorsModel): Observable<ResultModel<AuthorsModel>> {

    let params = this.GenerateRoutingParams(model);
    const route = `authormanagment/get${params}`;

    return this.httphelper.Get<AuthorsModel>(`${route}`);
  }

  public GetAllAuthors(): Observable<ResultModel<AllAuthorsModel>> {
    const route = `authormanagment/get/all`;
    return this.httphelper.Get<AllAuthorsModel>(`${route}`);
  }

  public RemoveAuthor(authorId: number): Observable<ResultModel> {
    const route = `authormanagment/remove/${authorId}`;
    return this.httphelper.Post(`${route}`);
  }

  public AddAuthor(authorModel: AddAuthorModel): Observable<ResultModel> {    
    const route = `authormanagment/add`;
    return this.httphelper.Post(`${route}`, authorModel);
  }

  public UpdateAuthor(authorModel: UpdateAuthorModel): Observable<ResultModel> {    
    const route = `authormanagment/update`;
    return this.httphelper.Post(`${route}`, authorModel);
  }

  private GenerateRoutingParams(model: GetAuthorsModel): string {
    const route = `?takeItems=${model.takeItems}&page=${model.page}&sortType=${model.sortBy}`;
    return route;
  }
}
