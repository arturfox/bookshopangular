import { Injectable } from '@angular/core';
import { HttpHelper } from '../helpers/httphelper';
import { ResultModel } from '../models/api/result-model';
import { Observable } from 'rxjs';
import { CheckoutSessionModel } from '../models/api/checkout/checkout-session-model';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  constructor(private httphelper: HttpHelper) {
  }

  public CreateCheckoutSession(): Observable<ResultModel<CheckoutSessionModel>> {
    const route = `checkout/create-session`;
    return this.httphelper.Get<CheckoutSessionModel>(`${route}`);
  }

  public CreateCheckoutSessionByOrderId(orderId: number): Observable<ResultModel<CheckoutSessionModel>> {
    const route = `checkout/create-session-for-order`;
    return this.httphelper.Get<CheckoutSessionModel>(`${route}?orderId=${orderId}`);
  }
}
