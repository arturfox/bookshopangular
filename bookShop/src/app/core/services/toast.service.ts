import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { TOAST_DURATION_MILLISECONDS } from '../../../environments/environment';
import { ResultModel } from '../models/api/result-model';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private _snackBar: MatSnackBar) {

  }

  showMessageFromResult<T>(result: ResultModel<T>) {

    if (result.errors != null && result.errors.length != 0) {

      this.showMessage(result.errors.toString());
      return;
    }

    this.showMessage(result.defaultErrorMessage);
  }

  showMessage(message: string) {
    this._snackBar.open(message, null, {
      duration: TOAST_DURATION_MILLISECONDS,
    });
  }

}
