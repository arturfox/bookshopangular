import { Injectable } from '@angular/core';
import { HttpHelper } from '../helpers/httphelper';
import { ResultModel } from '../models/api/result-model';
import { Observable } from 'rxjs';
import { GetUsersModel } from '../models/api/user-managment/get-users-model';
import { UsersModel } from '../models/api/user-managment/users-model';
import { UpdateUserModel } from '../models/api/user-managment/update-user-model';

@Injectable({
  providedIn: 'root'
})
export class UserManagmentService {

  constructor(private httphelper: HttpHelper) {
  }

  public GetUsers(model: GetUsersModel): Observable<ResultModel<UsersModel>> {

    let params = this.GenerateRoutingParams(model);
    const route = `usermanagment/get${params}`;

    return this.httphelper.Get<UsersModel>(`${route}`);
  }

  public SuspendUserAccount(userId: number): Observable<ResultModel> {
    const route = `usermanagment/suspend/${userId}`;
    return this.httphelper.Post(`${route}`);
  }

  public ActivateUserAccount(userId: number): Observable<ResultModel> {
    const route = `usermanagment/activate/${userId}`;
    return this.httphelper.Post(`${route}`);
  }

  public RemoveUser(userId: number): Observable<ResultModel> {
    const route = `usermanagment/remove/${userId}`;
    return this.httphelper.Post(`${route}`);
  }

  public UpdateUser(model: UpdateUserModel): Observable<ResultModel> {
    const route = `usermanagment/update/`;
    return this.httphelper.Post(`${route}`,model);
  }

  private GenerateRoutingParams(model: GetUsersModel): string {

    let route = `?takeItems=${model.takeItems}&page=${model.page}&sortBy=${model.sortBy}`;

    if (model.searchQuery !== undefined && model.searchQuery !== "%" && model.searchQuery !== "") {
      route = `${route}&searchQuery=${model.searchQuery}`;
    }

    if (model.accountStates === undefined || model.accountStates.length === 0) {
      return route;
    }

    route = `${route}&acсountStates=`;

    route = `${route}${model.accountStates[0]}`;

    if (model.accountStates.length === 1) {
      return route;
    }

    for (let i = 1; i < model.accountStates.length; i++) {

      route = `${route},${model.accountStates[i]}`;
    }

    return route;
  }
}
