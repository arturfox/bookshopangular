import { Injectable } from '@angular/core';
import { HttpHelper } from '../helpers/httphelper';
import { ResultModel } from '../models/api/result-model';
import { Observable } from 'rxjs';
import { GetOrdersModel } from '../models/api/order-managment/get-orders-model';
import { OrdersModel } from '../models/api/order-managment/orders-model';

@Injectable({
  providedIn: 'root'
})
export class OrderManagmentService {

  constructor(private httphelper: HttpHelper) {
  }

  public GetOrders(model: GetOrdersModel): Observable<ResultModel<OrdersModel>> {

    let params = this.GenerateRoutingParams(model);
    const route = `ordermanagment/get${params}`;

    return this.httphelper.Get<OrdersModel>(`${route}`);
  }

  private GenerateRoutingParams(model: GetOrdersModel): string {

    let route = `?takeItems=${model.takeItems}&page=${model.page}&sortBy=${model.sortBy}`;

    if (model.orderStates === undefined || model.orderStates.length === 0) {
      return route;
    }

    route = `${route}&orderStates=`;

    route = `${route}${model.orderStates[0]}`;

    if (model.orderStates.length === 1) {
      return route;
    }

    for (let i = 1; i < model.orderStates.length; i++) {

      route = `${route},${model.orderStates[i]}`;
    }

    return route;
  }
}
