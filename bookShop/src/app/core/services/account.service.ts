import { Injectable } from '@angular/core';
import { HttpHelper } from '../helpers/httphelper';
import { ResultModel } from '../models/api/result-model';
import { Observable } from 'rxjs';
import { LoginModel } from '../models/api/authentication/login-model';
import { LoginResultModel } from '../models/api/authentication/login-result-model';
import { CreateAccountModel } from '../models/api/authentication/create-account-model';
import { RecoverPasswordModel } from '../models/api/authentication/recover-password-model';
import { ProfileModel } from '../models/api/authentication/profile-model';
import { UpdateProfileModel } from '../models/api/authentication/update-profile-model';
import { RefreshTokenModel } from '../models/api/authentication/refresh-token-model';

@Injectable({ providedIn: 'root' })
export class AccountService {

    constructor(private httphelper: HttpHelper) {}

    public login(model: LoginModel): Observable<ResultModel<LoginResultModel>> {
        const route = "account/login/";
        return this.httphelper.Post<LoginResultModel>(`${route}`, model);
    }

    public createAccount(model: CreateAccountModel): Observable<ResultModel>{
        const route = "account/createaccount/";
        return this.httphelper.Post<void>(`${route}`, model);
    }

    public recoverPassword(model: RecoverPasswordModel): Observable<ResultModel>{
        const route = "account/recoverPassword/";
        return this.httphelper.Post<void>(`${route}`, model);
    }

    public logout(): Observable<ResultModel>{
        const route = "account/logout/";
        return this.httphelper.Post<void>(`${route}`);
    }

    public GetProfile(): Observable<ResultModel<ProfileModel>>{
        const route = "account/profile/";
        return this.httphelper.Get<ProfileModel>(`${route}`);
    }

    public UpdateProfile(model: UpdateProfileModel): Observable<ResultModel>{
        const route = "account/profile/update/";
        return this.httphelper.Post(`${route}`, model);
    }
    
    public RefreshToken(model: RefreshTokenModel): Observable<ResultModel<LoginResultModel>> {
        const route = "account/token/refresh/";
        return this.httphelper.Post<LoginResultModel>(`${route}`, model);
    }
} 