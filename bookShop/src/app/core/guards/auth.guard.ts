import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { IAppState } from '../../core/store/state/app.state';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserRole } from '../enums/user-role.enum';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router,
    private _store: Store<IAppState>) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

    return this._store.select(appState => appState.account.currentUser)
      .pipe(map(currentUser => {
        let roles = route.data.roles as Array<UserRole>;
        if (roles === undefined || roles.length == 0) {
          if (currentUser === null) {
            this.router.navigate(['login']);
          }
          return (currentUser !== null);
        }

        return roles.includes(currentUser.userRole);
      }))
  }

}
