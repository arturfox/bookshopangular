import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { IAppState } from '../../core/store/state/app.state';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OpposedAuthGuard implements CanActivate {
  constructor(private router: Router,
    private _store: Store<IAppState>) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this._store.select(appState => appState.account.currentUser)
      .pipe(map(currentUser => {
        if (currentUser !== null) {
          this.router.navigate(['catalog']);
        }
        return (currentUser === null);
      }))
  }
}
