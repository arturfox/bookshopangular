import { TestBed, async, inject } from '@angular/core/testing';

import { OpposedAuthGuard } from './opposed-auth.guard';

describe('OpposedAuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OpposedAuthGuard]
    });
  });

  it('should ...', inject([OpposedAuthGuard], (guard: OpposedAuthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
