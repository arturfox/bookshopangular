import { Injectable } from '@angular/core';
import { SERVER_URL } from '../../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs/';
import { catchError } from 'rxjs/operators';
import { ResultModel } from '../models/api/result-model';


@Injectable({ providedIn: 'root' })
export class HttpHelper {

    constructor(private client: HttpClient) {

    }

    public Post<T>(query: string, content: any = null, handleException = true): Observable<ResultModel<T>> {

        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        headers = headers.append('Accept', 'application/json');

        if (handleException) {
            return this.client.post<ResultModel<T>>(`${SERVER_URL}${query}`, content, { headers: headers, responseType: "json" })
                .pipe(catchError(() => { return of(this.getErrorModel<T>(query)); }));
        }

        return this.client.post<ResultModel<T>>(`${SERVER_URL}${query}`, content, { headers: headers, responseType: "json" });
    }

    public Get<T>(query: string, handleException = true): Observable<ResultModel<T>> {

        if (handleException) {
            return this.client.get<ResultModel<T>>(`${SERVER_URL}${query}`).pipe(catchError(() => { return of(this.getErrorModel<T>(query)); }));
        }

        return this.client.get<ResultModel<T>>(`${SERVER_URL}${query}`);
    }


    private getErrorModel<T>(route: string): ResultModel<T> {

        let errorModel = new ResultModel<T>();
        errorModel.isSucceed = false;
        errorModel.defaultErrorMessage = `en unknow error occurred while refering to ${route}`;

        return errorModel;
    }
}
