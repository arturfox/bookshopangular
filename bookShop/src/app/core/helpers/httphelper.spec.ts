import { TestBed } from '@angular/core/testing';

import { HttpHelper } from './httphelper';

describe('HttpHelper', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpHelper = TestBed.get(HttpHelper);
    expect(service).toBeTruthy();
  });
});
