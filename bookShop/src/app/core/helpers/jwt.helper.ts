import { Injectable } from '@angular/core';
import { KJUR } from 'jsrsasign';
import { b64utoutf8 } from 'jsrsasign';

@Injectable({
  providedIn: 'root'
})
export class JwtHelper {

  constructor() { }

  public getPayloadDataFromJWT(jwt): any {
    return KJUR.jws.JWS.readSafeJSONString(b64utoutf8(jwt.split(".")[1]));
  }
}
