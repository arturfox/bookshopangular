import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { AccountService } from './services/account.service';
import { appReducers } from './store/reducers/app.reducers';
import { environment } from '../../environments/environment';
import { AccountEffects } from './store/effects/account.effects';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AcessTokenInterceptor } from './interceptors/access-token.interceptor';
import { CatalogEffects } from './store/effects/catalog.effects';
import { CatalogService } from './services/catalog.service';
import { CartService } from './services/cart.service';
import { CartEffects } from './store/effects/cart.effects';
import { MatSnackBarModule } from '@angular/material';
import { storageMetaReducer } from './store/reducers/storage.metareducer';
import { OrdersEffects } from './store/effects/orders.effects';
import { UserManagmentEffects } from './store/effects/user-managment.effects';
import { CheckoutService } from './services/checkout.service';
import { OrdersService } from './services/orders.service';
import { UserManagmentService } from './services/user-managment.service';
import { ToastService } from './services/toast.service';
import { OrderManagmentEffects } from './store/effects/order-managment.effects';
import { AuthorManagmentEffects } from './store/effects/author-managment.effects';
import { AuthorManagmentService } from './services/author-managment.service';
import { CatalogManagmentEffects } from './store/effects/catalog-managment.effects';
import { RefreshTokenInterceptor } from './interceptors/refresh-token.interceptor';
//import {enableBatchReducer} from 'ngrx-batch-action-reducer'

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatSnackBarModule,
    StoreModule.forRoot(appReducers, {
      metaReducers: [storageMetaReducer/*, enableBatchReducer*/]
    }),
    EffectsModule.forRoot([AccountEffects,
      CatalogEffects,
      CartEffects,
      OrdersEffects,
      UserManagmentEffects,
      OrderManagmentEffects,
      AuthorManagmentEffects,
      CatalogManagmentEffects]),
    StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  providers: [
    AccountService,
    CatalogService,
    CartService,
    CheckoutService,
    OrdersService,
    UserManagmentService,
    ToastService,
    AuthorManagmentService,
    { provide: HTTP_INTERCEPTORS, useClass: AcessTokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: RefreshTokenInterceptor, multi: true }
  ]
})
export class CoreModule { }
