import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterComponent } from './filter/filter.component';
import { CatalogComponent } from './catalog/catalog.component';
import { ViewCatalogComponent } from './view-catalog/view-catalog.component';
import { PrintingEditionRoutingModule } from './printing-edition-routing.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule, MatCardModule, MatCheckboxModule, MatInputModule, MatSelectModule } from '@angular/material';
import { FlexLayoutModule } from "@angular/flex-layout";
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { SharedModule } from '../shared/shared.module';
import { MatDividerModule } from '@angular/material/divider';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { CatalogDetailsComponent } from './catalog-details/catalog-details.component';

@NgModule({
  declarations: [FilterComponent, CatalogComponent, ViewCatalogComponent, CatalogDetailsComponent],
  imports: [
    CommonModule,
    PrintingEditionRoutingModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    FlexLayoutModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    SharedModule,
    MatCheckboxModule,
    MatDividerModule,
    NgxSliderModule,
    FormsModule,
    MatInputModule,
    MatSelectModule,
    NgxPaginationModule
  ]
})
export class PrintingEditionModule { }
