import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { IAppState } from '../../core/store/state/app.state';
import { GetCatalogDetails } from '../../core/store/actions/catalog.actions';
import { selectCatalogDetails } from '../../core/store/selectors/catalog.selectors';
import { AddToCartModel } from '../../core/models/api/cart/add-to-cart-model';
import { AddToCart } from '../../core/store/actions/cart.actions';
import { isAddedToCart } from 'src/app/core/store/selectors/cart.selectors';
import { UserRole } from '../../core/enums/user-role.enum';
import { selectRole } from '../../core/store/selectors/account.selectors';

@Component({
  selector: 'app-catalog-details',
  templateUrl: './catalog-details.component.html',
  styleUrls: ['./catalog-details.component.scss']
})
export class CatalogDetailsComponent implements OnInit {

  @ViewChild('QtyInput', { static: false }) qtyInput: ElementRef;

  details$ = this._store.pipe(select(selectCatalogDetails));
  isAddedToCart$ = this._store.pipe(select(state => isAddedToCart(state, { printingEditionId: this.id})));

  id: number;

  roles: any = UserRole;
  selectRole$ = this._store.pipe(select(selectRole));

  constructor(private _route: ActivatedRoute,
    private _store: Store<IAppState>) {

    this._route.params.subscribe(params => {
      
      this.id = +params['id'];
      this._store.dispatch(new GetCatalogDetails(this.id)); 
    });
  }

  ngOnInit() {

  }
  
  onAddToCart(){
    let model = new AddToCartModel();
    model.positionId = this.id;
    model.quantity = +this.qtyInput.nativeElement.value;

    this._store.dispatch(new AddToCart(model));
  }
}
