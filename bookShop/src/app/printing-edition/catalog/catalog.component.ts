import { Component, ViewChild, OnInit, ElementRef, AfterViewInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { IAppState } from '../../core/store/state/app.state';
import { Store, select } from '@ngrx/store';
import { selectCatalog, selectPagination } from '../../core/store/selectors/catalog.selectors';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { SEARCH_DELAY_MILLISECONDS } from 'src/environments/environment';
import { SortType } from '../../core/enums/sort-type.enum';

@Component({
  selector: 'printing-edition-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('Search', { static: false }) searchField: ElementRef;
  @Output() searchApplied = new EventEmitter<string>();
  @Output() sortTypeChanged = new EventEmitter<SortType>();
  @Output() pageChanged = new EventEmitter<number>();

  catalog$ = this._store.pipe(select(selectCatalog));

  pagination$ = this._store.pipe(select(selectPagination));

  pagination = { itemsPerPage: 0, 
    currentPage: 0,
    totalItems: 0,
    id: "first"} ;

  gridColumns = 4;

  searchSubscription: Subscription;
  searchQuery$: Observable<any>;

  sortArray: { id: number; name: string }[] = [];
  selected: string;

  constructor(private _store: Store<IAppState>) {

    for (var n in SortType) {
      if (typeof SortType[n] === 'number'
        && SortType[n] != SortType.None.toString()) {
        this.sortArray.push({ id: <any>SortType[n], name: n });
      }
    }

    this.selected = this.sortArray[1].name;

    this.pagination$.subscribe((data) => { 

      if(data === null){
        return;
      }

      this.pagination = data;
    });
  }
  
  ngOnInit() {

  }

  ngAfterViewInit(): void {

    this.searchQuery$ = fromEvent(this.searchField.nativeElement, "input")
      .pipe(map(x => (x as any).target.value),
        debounceTime(SEARCH_DELAY_MILLISECONDS));

    this.searchSubscription = this.searchQuery$.subscribe((data) => {
      this.searchApplied.emit(this.normalizeQuery(data));
    });

  }

  ngOnDestroy(): void {

    this.searchSubscription.unsubscribe();
  }

  normalizeQuery(query: string): string {

    query = query.replace("%", "");
    query = query.toUpperCase().trim();
    query = query.replace(" ", "%");

    return query;
  }

  onSortTypeChanged() {
    let sortType = SortType[this.selected];
    this.sortTypeChanged.emit(sortType);
  }

  handlePageChange(data: number) {
    this.pageChanged.emit(data);
  }
}
