import { Component, OnInit, ViewChildren, QueryList, ElementRef, Output, EventEmitter } from '@angular/core';
import { PrintingEditionType } from '../../core/enums/printing-edition-type.enum';
import { Options } from "@angular-slider/ngx-slider";
import { MIN_PRICE_VALUE, MAX_PRICE_VALUE } from '../../../environments/environment';
import { MatCheckbox } from '@angular/material';
import { FilterModel } from '../../core/models/filter-model';

@Component({
  selector: 'printing-edition-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  @Output() filterApplied = new EventEmitter<FilterModel>();

  @ViewChildren('categoryCheckbox') categoryCheckBoxes: QueryList<MatCheckbox>

  categories: { id: number; name: string }[] = [];

  value: number = MIN_PRICE_VALUE;
  highValue: number = MAX_PRICE_VALUE;

  options: Options = {
    floor: MIN_PRICE_VALUE,
    ceil: MAX_PRICE_VALUE
  };

  constructor() {

    for (var n in PrintingEditionType) {
      if (typeof PrintingEditionType[n] === 'number'
        && PrintingEditionType[n] != PrintingEditionType.None.toString()) {
        this.categories.push({ id: <any>PrintingEditionType[n], name: n });
      }
    }

  }

  ngOnInit() {
  }

  onSelectCriteria(event: any) {
    this.invokeFilter();
  }

  onSubmit(event: any) {

    this.invokeFilter();
  }

  invokeFilter() {

    let filterModel = new FilterModel();
    if (this.value !== MIN_PRICE_VALUE) {
      filterModel.minValue = this.value;
    }
    if (this.highValue !== MAX_PRICE_VALUE) {
      filterModel.maxValue = this.highValue;
    }

    this.categoryCheckBoxes.forEach((element) => {
      if (!element.checked) {
        return;
      }

      if (filterModel.categories === undefined) {
        filterModel.categories = new Array<PrintingEditionType>();
      }

      filterModel.categories.push(PrintingEditionType[element.name]);
    });

    this.filterApplied.emit(filterModel);
  }
}
