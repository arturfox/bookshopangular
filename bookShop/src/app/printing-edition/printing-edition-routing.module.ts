import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewCatalogComponent } from './view-catalog/view-catalog.component';
import { CatalogDetailsComponent } from './catalog-details/catalog-details.component';

const routes: Routes = [
    { path: '', component: ViewCatalogComponent},
    { path: 'details/:id', component: CatalogDetailsComponent}
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class PrintingEditionRoutingModule { }