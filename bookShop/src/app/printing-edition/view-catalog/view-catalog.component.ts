import { Component, OnInit } from '@angular/core';
import { IAppState } from '../../core/store/state/app.state';
import { Store } from '@ngrx/store';
import { GetCatalog } from '../../core/store/actions/catalog.actions';
import { GetCatalogModel } from '../../core/models/api/catalog/get-catalog-model';
import { MAX_ITEMS_COUNT_PER_PAGE } from '../../../environments/environment';
import { FilterModel } from '../../core/models/filter-model';
import { SortType } from '../../core/enums/sort-type.enum';
import { GetCart } from '../../core/store/actions/cart.actions';

@Component({
  selector: 'app-view-catalog',
  templateUrl: './view-catalog.component.html',
  styleUrls: ['./view-catalog.component.scss']
})
export class ViewCatalogComponent implements OnInit {

  filter: GetCatalogModel;

  constructor(private _store: Store<IAppState>) { }

  onFilterApplied(event: FilterModel) {

    this.filter = Object.assign({}, this.filter, {
      "categories": event.categories,
      "minValue": event.minValue,
      "maxValue": event.maxValue,
    });

    this._store.dispatch(new GetCatalog(this.filter));
  }

  onSearchApplied(event: string) {

    this.filter = Object.assign({}, this.filter, {
      "searchQuery": event
    });

    this._store.dispatch(new GetCatalog(this.filter));
  }

  onSortTypeChanged(event: SortType){

    this.filter = Object.assign({}, this.filter, {
      "orderBy": event
    });

    this._store.dispatch(new GetCatalog(this.filter));
  }

  onPageChanged(event: number){

    this.filter = Object.assign({}, this.filter, {
      "page": event - 1
    });

    this._store.dispatch(new GetCatalog(this.filter));
  }

  ngOnInit() {

    this.filter = new GetCatalogModel();
    this.filter.page = 0;
    this.filter.takeItems = MAX_ITEMS_COUNT_PER_PAGE;
    this.filter.orderBy = SortType.HighToLow;

    this._store.dispatch(new GetCatalog(this.filter));

    //todo: move get cart functionality
    this._store.dispatch(new GetCart());
  }
}
