import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, ViewChildren, QueryList, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IAppState } from '../../core/store/state/app.state';
import { MatDialog, MatCheckbox } from '@angular/material';
import { selectUsers, selectPagination } from '../../core/store/selectors/user-managment.selectors';
import { AccountStatus } from '../../core/enums/account-status.enum';
import { GetUsersModel } from '../../core/models/api/user-managment/get-users-model';
import { MAX_ITEMS_COUNT_PER_PAGE, SEARCH_DELAY_MILLISECONDS } from '../../../environments/environment';
import { UserManagmentSortType } from '../../core/enums/user-managment-sort-type.enum';
import { GetUsers, SuspendUserAccount, ActivateUserAccount, RemoveUser } from '../../core/store/actions/user-managment.actions';
import { UsersModelItem } from '../../core/models/api/user-managment/users-model-item';
import { ActionDialogComponent } from '../../shared/components/action-dialog/action-dialog.component';
import { Subscription, Observable, fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { EditUserDialogComponent } from '../edit-user-dialog/edit-user-dialog.component';

@Component({
  selector: 'app-user-managment',
  templateUrl: './user-managment.component.html',
  styleUrls: ['./user-managment.component.scss']
})
export class UserManagmentComponent implements OnInit, AfterViewInit, OnDestroy {

  //account status filter
  @ViewChildren('categoryCheckbox') categoryCheckBoxes: QueryList<MatCheckbox>
  categories: { id: number; name: string }[] = [];

  //search user
  @ViewChild('Search', { static: false }) searchField: ElementRef;
  searchSubscription: Subscription;
  searchQuery$: Observable<any>;

  users$ = this._store.pipe(select(selectUsers));
  displayedColumns: string[] = ['fullName', 'userName', 'email', 'status', 'actions'];
  accountStatus: any = AccountStatus;

  filter: GetUsersModel;

  pagination$ = this._store.pipe(select(selectPagination));

  pagination = {
    itemsPerPage: 0,
    currentPage: 0,
    totalItems: 0,
    id: "first"
  };

  constructor(private _store: Store<IAppState>,
    public dialog: MatDialog) {
    this.pagination$.subscribe((data) => {
      if (data === null) {
        return;
      }
      this.pagination = data;
    });

    for (var n in AccountStatus) {
      if (typeof AccountStatus[n] === 'number'
        && AccountStatus[n] != AccountStatus.None.toString()) {
        this.categories.push({ id: <any>AccountStatus[n], name: n });
      }
    }
  }

  ngOnInit() {
    this.filter = new GetUsersModel();
    this.filter.page = 0;
    this.filter.takeItems = MAX_ITEMS_COUNT_PER_PAGE;
    this.filter.sortBy = UserManagmentSortType.UserNameAsc;

    this._store.dispatch(new GetUsers(this.filter));
  }

  handlePageChange(data: number) {
    this.filter = Object.assign({}, this.filter, {
      "page": data - 1
    });

    this._store.dispatch(new GetUsers(this.filter));
  }

  onStatusChanged(element: UsersModelItem, event: any) {

    if (element.accountStatus === AccountStatus.Active) {

      const dialogRef = this.dialog.open(ActionDialogComponent, {
        minWidth: '400px',
        data: { title: "Suspend user", subTitle: `Are you sure you want to suspend ${element.email} ?` }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this._store.dispatch(new SuspendUserAccount(element.id));
          return;
        }

        event.source.checked = !event.source.checked;
      });

      return;
    }

    const dialogRef = this.dialog.open(ActionDialogComponent, {
      minWidth: '400px',
      data: { title: "Activate user", subTitle: `Are you sure you want to activate ${element.email} ?` }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._store.dispatch(new ActivateUserAccount(element.id));
      }
    });
  }

  removeUser(element: UsersModelItem) {

    const dialogRef = this.dialog.open(ActionDialogComponent, {
      minWidth: '400px',
      data: { title: "Remove user", subTitle: `Are you sure you want to delete ${element.email} ?` }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._store.dispatch(new RemoveUser(element.id));
      }
    });
  }

  editUser(element: UsersModelItem) {

    const dialogRef = this.dialog.open(EditUserDialogComponent, {
      minWidth: '500px',
      data: element
    });
  }

  ngAfterViewInit(): void {

    this.searchQuery$ = fromEvent(this.searchField.nativeElement, "input")
      .pipe(map(x => (x as any).target.value),
        debounceTime(SEARCH_DELAY_MILLISECONDS));

    this.searchSubscription = this.searchQuery$.subscribe((data) => {
      this.onSearchApplied(this.normalizeQuery(data));
    });

  }

  ngOnDestroy(): void {

    this.searchSubscription.unsubscribe();
  }

  normalizeQuery(query: string): string {

    query = query.replace("%", "");
    query = query.toUpperCase().trim();
    query = query.replace(" ", "%");

    return query;
  }

  onSearchApplied(event: string) {
    this.filter = Object.assign({}, this.filter, {
      "searchQuery": event
    });

    this._store.dispatch(new GetUsers(this.filter));
  }

  onSelectCriteria(event: any) {
    let states = new Array<AccountStatus>();
    this.categoryCheckBoxes.forEach((element) => {
      if (!element.checked) {
        return;
      }
      states.push(AccountStatus[element.name]);
    });
    this.filter = Object.assign({}, this.filter, {
      "accountStates": states
    });

    this._store.dispatch(new GetUsers(this.filter));
  }

  onUserNameSortChanged() {

    let criteria = (this.filter.sortBy === UserManagmentSortType.UserNameAsc)
      ? UserManagmentSortType.UserNameDesc
      : UserManagmentSortType.UserNameAsc;

      this.updateCriteria(criteria);
  }

  onEmailSortChanged() {

    let criteria = (this.filter.sortBy === UserManagmentSortType.EmailAsc)
      ? UserManagmentSortType.EmailDesc
      : UserManagmentSortType.EmailAsc;

    this.updateCriteria(criteria);
  }

  updateCriteria(criteria: UserManagmentSortType){
    this.filter = Object.assign({}, this.filter, {
      "sortBy": criteria
    });

    this._store.dispatch(new GetUsers(this.filter));
  }
}
