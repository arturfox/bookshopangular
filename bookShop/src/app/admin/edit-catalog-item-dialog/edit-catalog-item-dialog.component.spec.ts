import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCatalogItemDialogComponent } from './edit-catalog-item-dialog.component';

describe('EditCatalogItemDialogComponent', () => {
  let component: EditCatalogItemDialogComponent;
  let fixture: ComponentFixture<EditCatalogItemDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCatalogItemDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCatalogItemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
