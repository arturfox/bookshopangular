import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PrintingEditionType } from '../../core/enums/printing-edition-type.enum';
import { Store, select, ActionsSubject } from '@ngrx/store';
import { IAppState } from '../../core/store/state/app.state';
import { GetAllAuthors, AddProduct, ECatalogManagmentActions, UpdateProduct } from '../../core/store/actions/catalog-managment.actions';
import { selectAllAuthors } from '../../core/store/selectors/catalog-managment.selectors';
import { Currency } from '../../core/enums/currency.enum';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ofType } from '@ngrx/effects';
import { Subscription } from 'rxjs';
import { CatalogModelItem } from '../../core/models/api/catalog-managment/catalog-model-item';
import { UpdateCatalogItemModel } from '../../core/models/api/catalog-managment/update-catalog-item-model';

@Component({
  selector: 'app-edit-catalog-item-dialog',
  templateUrl: './edit-catalog-item-dialog.component.html',
  styleUrls: ['./edit-catalog-item-dialog.component.scss']
})
export class EditCatalogItemDialogComponent implements OnInit, OnDestroy {

  addProductFormGroup: FormGroup;

  allAuthors$ = this._store.pipe(select(selectAllAuthors));

  typeArray: { id: number; name: string }[] = [];
  selectedType: string;

  currencyArray: { id: number; name: string }[] = [];
  selectedCurrency: string;

  imageSource: string;

  selectedAuthors: string[];

  actionsSubscription = new Subscription();

  constructor(private _formBuilder: FormBuilder,
    private _store: Store<IAppState>,
    public dialogRef: MatDialogRef<EditCatalogItemDialogComponent>,
    private actionsSubject: ActionsSubject,
    @Inject(MAT_DIALOG_DATA) public data: CatalogModelItem) {

    for (var n in PrintingEditionType) {
      if (typeof PrintingEditionType[n] === 'number'
        && PrintingEditionType[n] != PrintingEditionType.None.toString()) {
        this.typeArray.push({ id: <any>PrintingEditionType[n], name: n });
      }
    }

    this.selectedType = this.typeArray[data.printingEditionType - 1].name;
    for (var n in Currency) {
      if (typeof Currency[n] === 'number'
        && Currency[n] != Currency.None.toString()) {
        this.currencyArray.push({ id: <any>Currency[n], name: n });
      }
    }
    this.selectedCurrency = this.currencyArray[data.currency - 1].name;

    this.actionsSubscription = this.actionsSubject.pipe(
      ofType(ECatalogManagmentActions.UpdateProductSuccess))
      .subscribe(data => {
        this.dialogRef.close();
      });

      this.imageSource = data.imageSource;
  }

  ngOnInit() {

    let authorsIds = new Array<string>();
    this.data.authors.forEach((author)=>{
        authorsIds.push(author.id.toString());
    });

    this.addProductFormGroup = this._formBuilder.group({
      title: [this.data.title, Validators.required],
      description: [this.data.description, Validators.required],
      price: [this.data.price, Validators.required],
      author: [authorsIds, Validators.required],
      category: [this.selectedType, Validators.required],
      currency: [{ value: this.selectedCurrency, disabled: false }, Validators.required],
    });

    this._store.dispatch(new GetAllAuthors());
  }

  onImageSourceUpdated(data: string) {
    this.imageSource = data;
  }

  onSubmit() {
    if (!this.addProductFormGroup.valid) {
      return;
    }

    let productModel = new UpdateCatalogItemModel();
    productModel.id = this.data.id;
    productModel.title = this.addProductFormGroup.get('title').value;
    productModel.description = this.addProductFormGroup.get('description').value;
    productModel.price = this.addProductFormGroup.get('price').value;
    productModel.currency = Currency[this.selectedCurrency];
    productModel.printingEditionType = PrintingEditionType[this.selectedType];
    productModel.imageSource = this.imageSource;

    productModel.authorsIds = new Array<number>();
    let authorsIds = <Array<string>>this.addProductFormGroup.get('author').value;
    authorsIds.forEach((authorId) => {
      productModel.authorsIds.push(+authorId);
    })
    
    this._store.dispatch(new UpdateProduct(productModel));

  }

  onCancelClick(){
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.actionsSubscription.unsubscribe();
  }
}
