import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserManagmentComponent } from './user-managment/user-managment.component';
import { CatalogManagmentComponent } from './catalog-managment/catalog-managment.component';
import { OrderManagmentComponent } from './order-managment/order-managment.component';
import { AuthorManagmentComponent } from './author-managment/author-managment.component';

const routes: Routes = [
    { path: '', redirectTo: 'user-managment' },
    { path: 'user-managment', component: UserManagmentComponent },
    { path: 'catalog-managment', component: CatalogManagmentComponent },
    { path: 'order-managment', component: OrderManagmentComponent },
    { path: 'author-managment', component: AuthorManagmentComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AdminRoutingModule { }