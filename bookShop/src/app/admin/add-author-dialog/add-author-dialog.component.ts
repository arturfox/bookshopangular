import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store, ActionsSubject } from '@ngrx/store';
import { IAppState } from '../../core/store/state/app.state';
import { MatDialogRef } from '@angular/material';
import { AddAuthorModel } from '../../core/models/api/author-managment/add-author-model';
import { AddAuthor, EAuthorManagmentActions } from '../../core/store/actions/author-managment.actions';
import { Subscription } from 'rxjs';
import { ofType } from '@ngrx/effects';

@Component({
  selector: 'app-add-author-dialog',
  templateUrl: './add-author-dialog.component.html',
  styleUrls: ['./add-author-dialog.component.scss']
})
export class AddAuthorDialogComponent implements OnInit, OnDestroy {

  addAuthorFormGroup: FormGroup;

  actionsSubscription = new Subscription();

  constructor(private _formBuilder: FormBuilder,
    private _store: Store<IAppState>,
    public dialogRef: MatDialogRef<AddAuthorDialogComponent>,
    private actionsSubject: ActionsSubject) { 

      this.actionsSubscription = this.actionsSubject.pipe(
        ofType(EAuthorManagmentActions.AddAuthorSuccess))
        .subscribe(data => {
          this.dialogRef.close();
        });
    }

  ngOnInit() {
    this.addAuthorFormGroup = this._formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required]
    });
  }

  onSubmit() {
    if (!this.addAuthorFormGroup.valid) {
      return;
    }

    let author = new AddAuthorModel();
    author.firstName = this.addAuthorFormGroup.get('firstName').value;
    author.lastName = this.addAuthorFormGroup.get('lastName').value;

    this._store.dispatch(new AddAuthor(author));
  }

  onCancelClick(){
    this.dialogRef.close();
  }
  
  ngOnDestroy() {
    this.actionsSubscription.unsubscribe();
  }
}
