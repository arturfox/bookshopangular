import { Component, OnInit } from '@angular/core';
import { selectAuthors, selectPagination } from '../../core/store/selectors/author-managment.selectors';
import { select, Store } from '@ngrx/store';
import { GetAuthorsModel } from '../../core/models/api/author-managment/get-authors-model';
import { MatDialog } from '@angular/material';
import { IAppState } from '../../core/store/state/app.state';
import { MAX_ITEMS_COUNT_PER_PAGE } from '../../../environments/environment';
import { GetAuthors, RemoveAuthor } from '../../core/store/actions/author-managment.actions';
import { AuthorsModelItem } from '../../core/models/api/author-managment/authors-model-item';
import { AuthorManagmentSortType } from '../../core/enums/author-managment-sort-type.enum';
import { ActionDialogComponent } from '../../shared/components/action-dialog/action-dialog.component';
import { AddAuthorDialogComponent } from '../add-author-dialog/add-author-dialog.component';
import { EditAuthorDialogComponent } from '../edit-author-dialog/edit-author-dialog.component';

@Component({
  selector: 'app-author-managment',
  templateUrl: './author-managment.component.html',
  styleUrls: ['./author-managment.component.scss']
})
export class AuthorManagmentComponent implements OnInit {

  authors$ = this._store.pipe(select(selectAuthors));
  displayedColumns: string[] = ['authorId', 'name', 'product', 'actions'];

  filter: GetAuthorsModel;

  pagination$ = this._store.pipe(select(selectPagination));

  pagination = {
    itemsPerPage: 0,
    currentPage: 0,
    totalItems: 0,
    id: "authormanagment"
  };

  constructor(private _store: Store<IAppState>,
    public dialog: MatDialog) {
    this.pagination$.subscribe((data) => {
      if (data === null) {
        return;
      }
      this.pagination = data;
    });
  }

  ngOnInit() {
    this.filter = new GetAuthorsModel();
    this.filter.page = 0;
    this.filter.takeItems = MAX_ITEMS_COUNT_PER_PAGE;
    this.filter.sortBy = AuthorManagmentSortType.NumberDesc;

    this._store.dispatch(new GetAuthors(this.filter));
  }

  handlePageChange(data: number) {
    this.filter = Object.assign({}, this.filter, {
      "page": data - 1
    });

    this._store.dispatch(new GetAuthors(this.filter));
  }

  removeAuthor(element: AuthorsModelItem) {
    const dialogRef = this.dialog.open(ActionDialogComponent, {
      minWidth: '400px',
      data: { title: "Remove author", subTitle: `Are you sure you want to delete ${element.firstName} ${element.lastName} ?` }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._store.dispatch(new RemoveAuthor(element.id));
      }
    });
  }

  editAuthor(element: AuthorsModelItem) {
    this.dialog.open(EditAuthorDialogComponent, {
      minWidth: '400px',
      data: element
    });
  }

  onAuthorIdSortChanged() {
    let criteria = (this.filter.sortBy === AuthorManagmentSortType.NumberDesc)
      ? AuthorManagmentSortType.NumberAsc
      : AuthorManagmentSortType.NumberAsc; 

    this.updateCriteria(criteria);
  }

  onUserNameSortChanged() {
    let criteria = (this.filter.sortBy === AuthorManagmentSortType.NameAsc)
      ? AuthorManagmentSortType.NameDesc
      : AuthorManagmentSortType.NameAsc;

    this.updateCriteria(criteria);
  }

  onCreateClicked() {
    this.dialog.open(AddAuthorDialogComponent, {
      minWidth: '400px'
    });
  }

  updateCriteria(criteria: AuthorManagmentSortType) {
    this.filter = Object.assign({}, this.filter, {
      "sortBy": criteria
    });

    this._store.dispatch(new GetAuthors(this.filter));
  }
}
