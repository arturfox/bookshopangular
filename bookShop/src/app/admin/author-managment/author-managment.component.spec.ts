import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorManagmentComponent } from './author-managment.component';

describe('AuthorManagmentComponent', () => {
  let component: AuthorManagmentComponent;
  let fixture: ComponentFixture<AuthorManagmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorManagmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorManagmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
