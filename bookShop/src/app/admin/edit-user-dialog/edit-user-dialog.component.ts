import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UsersModelItem } from '../../core/models/api/user-managment/users-model-item';
import { Store, ActionsSubject } from '@ngrx/store';
import { IAppState } from '../../core/store/state/app.state';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators, ConfirmValidParentMatcher } from '../../core/validators/custom-validators';
import { UpdateUserModel } from '../../core/models/api/user-managment/update-user-model';
import { UpdateUser, EUserManagmentActions } from '../../core/store/actions/user-managment.actions';
import { Subscription } from 'rxjs';
import { ofType } from '@ngrx/effects';

@Component({
  selector: 'app-edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.scss']
})
export class EditUserDialogComponent implements OnInit, OnDestroy {

  editUserFormGroup: FormGroup;
  confirmValidParentMatcher = new ConfirmValidParentMatcher();

  actionsSubscription = new Subscription();

  constructor(@Inject(MAT_DIALOG_DATA) public data: UsersModelItem,
    private _formBuilder: FormBuilder,
    private _store: Store<IAppState>,
    public dialogRef: MatDialogRef<EditUserDialogComponent>,
    private actionsSubject: ActionsSubject) {
      
    this.actionsSubscription = this.actionsSubject.pipe(
      ofType(EUserManagmentActions.UpdateUserSuccess))
      .subscribe(data => {
        this.dialogRef.close();
      });
     }

  ngOnInit() {
    this.editUserFormGroup = this._formBuilder.group({
      firstName: [this.data.firstName, Validators.required],
      lastName: [this.data.lastName, Validators.required],
      userName: [this.data.userName, Validators.required],
      email: [this.data.email, Validators.required],
      passwordGroup: this._formBuilder.group({
        password: [''],
        confirmPassword: ['']
      }, { validator: CustomValidators.childrenEqual })
    });
  }

  onSubmit() {
    if (!this.editUserFormGroup.valid) {
      return;
    }

    let updateModel = new UpdateUserModel();
    updateModel.id = this.data.id;
    updateModel.firstName = this.editUserFormGroup.get('firstName').value;
    updateModel.lastName = this.editUserFormGroup.get('lastName').value;
    updateModel.userName = this.editUserFormGroup.get('userName').value;
    updateModel.email = this.editUserFormGroup.get('email').value;
    updateModel.password = this.editUserFormGroup.get(['passwordGroup','password']).value;
    updateModel.confirmPassword = this.editUserFormGroup.get(['passwordGroup','confirmPassword']).value;

    this._store.dispatch(new UpdateUser(updateModel));
  }

  onCancelClick(){
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.actionsSubscription.unsubscribe();
  }
}
