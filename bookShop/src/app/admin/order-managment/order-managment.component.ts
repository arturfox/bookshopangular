import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { selectOrders, selectPagination } from '../../core/store/selectors/order-managment.selectors';
import { GetOrdersModel } from '../../core/models/api/order-managment/get-orders-model';
import { IAppState } from '../../core/store/state/app.state';
import { MAX_ITEMS_COUNT_PER_PAGE } from '../../../environments/environment';
import { OrderManagmentSortType } from '../../core/enums/order-managment-sort-type.enum';
import { GetOrders } from '../../core/store/actions/order-managment.actions';
import { OrderStatus } from '../../core/enums/order-status.enum';
import { MatCheckbox } from '@angular/material';

@Component({
  selector: 'app-order-managment',
  templateUrl: './order-managment.component.html',
  styleUrls: ['./order-managment.component.scss']
})
export class OrderManagmentComponent implements OnInit {

  //order status filter
  @ViewChildren('categoryCheckbox') categoryCheckBoxes: QueryList<MatCheckbox>
  categories: { id: number; name: string }[] = [];

  orders$ = this._store.pipe(select(selectOrders));
  displayedColumns: string[] = ['order', 'date', 'userName', 'email', 'product', 'title', 'quantity', 'orderAmount', 'orderStatus'];
  orderStatus: any = OrderStatus;

  filter: GetOrdersModel;

  pagination$ = this._store.pipe(select(selectPagination));

  pagination = {
    itemsPerPage: 0,
    currentPage: 0,
    totalItems: 0,
    id: "first"
  };

  constructor(private _store: Store<IAppState>) {
    this.pagination$.subscribe((data) => {
      if (data === null) {
        return;
      }
      this.pagination = data;
    });

    for (var n in OrderStatus) {
      if (typeof OrderStatus[n] === 'number'
        && OrderStatus[n] != OrderStatus.None.toString()) {
        this.categories.push({ id: <any>OrderStatus[n], name: n });
      }
    }
  }

  ngOnInit() {
    this.filter = new GetOrdersModel();
    this.filter.page = 0;
    this.filter.takeItems = MAX_ITEMS_COUNT_PER_PAGE;
    this.filter.sortBy = OrderManagmentSortType.NumberDesc;

    this._store.dispatch(new GetOrders(this.filter));
  }

  handlePageChange(data: number) {
    this.filter = Object.assign({}, this.filter, {
      "page": data - 1
    });

    this._store.dispatch(new GetOrders(this.filter));
  }

  onSelectCriteria(event: any) {
    let states = new Array<OrderStatus>();
    this.categoryCheckBoxes.forEach((element) => {
      if (!element.checked) {
        return;
      }
      states.push(OrderStatus[element.name]);
    });
    this.filter = Object.assign({}, this.filter, {
      "orderStates": states
    });

    this._store.dispatch(new GetOrders(this.filter));
  }

  onDateSortChanged() {

    let criteria = (this.filter.sortBy === OrderManagmentSortType.DateAsc)
      ? OrderManagmentSortType.DateDesc
      : OrderManagmentSortType.DateAsc;

    this.updateCriteria(criteria);
  }

  onOrderIdSortChanged() {

    let criteria = (this.filter.sortBy === OrderManagmentSortType.NumberAsc)
      ? OrderManagmentSortType.NumberDesc
      : OrderManagmentSortType.NumberAsc;

    this.updateCriteria(criteria);
  }

  onOrderAmountSortChanged() {

    let criteria = (this.filter.sortBy === OrderManagmentSortType.AmountAsc)
      ? OrderManagmentSortType.AmountDesc
      : OrderManagmentSortType.AmountAsc;

    this.updateCriteria(criteria);
  }

  updateCriteria(criteria: OrderManagmentSortType) {
    this.filter = Object.assign({}, this.filter, {
      "sortBy": criteria
    });

    this._store.dispatch(new GetOrders(this.filter));
  }
}
