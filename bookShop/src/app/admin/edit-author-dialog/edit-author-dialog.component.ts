import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store, ActionsSubject } from '@ngrx/store';
import { IAppState } from '../../core/store/state/app.state';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UpdateAuthor, EAuthorManagmentActions } from '../../core/store/actions/author-managment.actions';
import { AuthorsModelItem } from '../../core/models/api/author-managment/authors-model-item';
import { UpdateAuthorModel } from '../../core/models/api/author-managment/update-author-model';
import { Subscription } from 'rxjs';
import { ofType } from '@ngrx/effects';

@Component({
  selector: 'app-edit-author-dialog',
  templateUrl: './edit-author-dialog.component.html',
  styleUrls: ['./edit-author-dialog.component.scss']
})
export class EditAuthorDialogComponent implements OnInit, OnDestroy {

  editAuthorFormGroup: FormGroup;

  actionsSubscription = new Subscription();

  constructor(private _formBuilder: FormBuilder,
    private _store: Store<IAppState>,
    public dialogRef: MatDialogRef<EditAuthorDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AuthorsModelItem,
    private actionsSubject: ActionsSubject) {

    this.actionsSubscription = this.actionsSubject.pipe(
      ofType(EAuthorManagmentActions.UpdateAuthorSuccess))
      .subscribe(data => {
        this.dialogRef.close();
      });
  }

  ngOnInit() {
    this.editAuthorFormGroup = this._formBuilder.group({
      firstName: [this.data.firstName, Validators.required],
      lastName: [this.data.lastName, Validators.required]
    });
  }

  onSubmit() {
    if (!this.editAuthorFormGroup.valid) {
      return;
    }

    let author = new UpdateAuthorModel();
    author.id = this.data.id;
    author.firstName = this.editAuthorFormGroup.get('firstName').value;
    author.lastName = this.editAuthorFormGroup.get('lastName').value;

    this._store.dispatch(new UpdateAuthor(author));
  }

  onCancelClick() {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.actionsSubscription.unsubscribe();
  }
}
