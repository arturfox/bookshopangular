import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PrintingEditionType } from '../../core/enums/printing-edition-type.enum';
import { Store, select, ActionsSubject } from '@ngrx/store';
import { IAppState } from '../../core/store/state/app.state';
import { GetAllAuthors, AddProduct, ECatalogManagmentActions } from '../../core/store/actions/catalog-managment.actions';
import { selectAllAuthors } from '../../core/store/selectors/catalog-managment.selectors';
import { Currency } from '../../core/enums/currency.enum';
import { AddCatalogItemModel } from '../../core/models/api/catalog-managment/add-catalog-item-model';
import { MatDialogRef } from '@angular/material';
import { ofType } from '@ngrx/effects';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-catalog-item-dialog',
  templateUrl: './add-catalog-item-dialog.component.html',
  styleUrls: ['./add-catalog-item-dialog.component.scss']
})
export class AddCatalogItemDialogComponent implements OnInit, OnDestroy {

  addProductFormGroup: FormGroup;

  allAuthors$ = this._store.pipe(select(selectAllAuthors));

  typeArray: { id: number; name: string }[] = [];
  selectedType: string;

  currencyArray: { id: number; name: string }[] = [];
  selectedCurrency: string;

  imageBase64: string;

  selectedAuthors: string[];

  actionsSubscription = new Subscription();

  constructor(private _formBuilder: FormBuilder,
    private _store: Store<IAppState>,
    public dialogRef: MatDialogRef<AddCatalogItemDialogComponent>,
    private actionsSubject: ActionsSubject) {

    for (var n in PrintingEditionType) {
      if (typeof PrintingEditionType[n] === 'number'
        && PrintingEditionType[n] != PrintingEditionType.None.toString()) {
        this.typeArray.push({ id: <any>PrintingEditionType[n], name: n });
      }
    }
    this.selectedType = this.typeArray[0].name;

    for (var n in Currency) {
      if (typeof Currency[n] === 'number'
        && Currency[n] != Currency.None.toString()) {
        this.currencyArray.push({ id: <any>Currency[n], name: n });
      }
    }
    this.selectedCurrency = this.currencyArray[0].name;

    this.actionsSubscription = this.actionsSubject.pipe(
      ofType(ECatalogManagmentActions.AddProductSuccess))
      .subscribe(data => {
        this.dialogRef.close();
      });
  }

  ngOnInit() {
    this.addProductFormGroup = this._formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      price: [0, Validators.required],
      author: ['', Validators.required],
      category: [this.selectedType, Validators.required],
      currency: [{ value: this.selectedCurrency, disabled: false }, Validators.required],
    });

    this._store.dispatch(new GetAllAuthors());
  }

  onImageSourceUpdated(data: string) {
    this.imageBase64 = data;
  }

  onSubmit() {
    if (!this.addProductFormGroup.valid) {
      return;
    }

    let productModel = new AddCatalogItemModel();
    productModel.title = this.addProductFormGroup.get('title').value;
    productModel.description = this.addProductFormGroup.get('description').value;
    productModel.price = this.addProductFormGroup.get('price').value;
    productModel.currency = Currency[this.selectedCurrency];
    productModel.printingEditionType = PrintingEditionType[this.selectedType];
    productModel.imageSource = this.imageBase64;

    productModel.authorsIds = new Array<number>();
    let authorsIds = <Array<string>>this.addProductFormGroup.get('author').value;
    authorsIds.forEach((authorId) => {
      productModel.authorsIds.push(+authorId);
    })
    
    this._store.dispatch(new AddProduct(productModel));

  }

  onCancelClick(){
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.actionsSubscription.unsubscribe();
  }
}
