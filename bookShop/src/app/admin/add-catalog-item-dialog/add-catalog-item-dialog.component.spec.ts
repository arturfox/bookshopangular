import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCatalogItemDialogComponent } from './add-catalog-item-dialog.component';

describe('AddCatalogItemDialogComponent', () => {
  let component: AddCatalogItemDialogComponent;
  let fixture: ComponentFixture<AddCatalogItemDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCatalogItemDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCatalogItemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
