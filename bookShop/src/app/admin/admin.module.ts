import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { UserManagmentComponent } from './user-managment/user-managment.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { MatTableModule } from '@angular/material/table';
import { SharedModule } from '../shared/shared.module';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatIconModule, MatInputModule, MatMenuModule, MatCheckboxModule, MatSelectModule } from '@angular/material';
import { EditUserDialogComponent } from './edit-user-dialog/edit-user-dialog.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CatalogManagmentComponent } from './catalog-managment/catalog-managment.component';
import { OrderManagmentComponent } from './order-managment/order-managment.component';
import { AuthorManagmentComponent } from './author-managment/author-managment.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AddAuthorDialogComponent } from './add-author-dialog/add-author-dialog.component';
import { EditAuthorDialogComponent } from './edit-author-dialog/edit-author-dialog.component';
import { AddCatalogItemDialogComponent } from './add-catalog-item-dialog/add-catalog-item-dialog.component';
import { EditCatalogItemDialogComponent } from './edit-catalog-item-dialog/edit-catalog-item-dialog.component';

@NgModule({
  declarations: [
    UserManagmentComponent,
    EditUserDialogComponent,
    CatalogManagmentComponent,
    OrderManagmentComponent,
    AuthorManagmentComponent,
    AddAuthorDialogComponent,
    EditAuthorDialogComponent,
    AddCatalogItemDialogComponent,
    EditCatalogItemDialogComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    NgxPaginationModule,
    MatTableModule,
    SharedModule,
    MatSlideToggleModule,
    FormsModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatSelectModule
  ],
  entryComponents: [
    EditUserDialogComponent,
    AddAuthorDialogComponent,
    EditAuthorDialogComponent,
    AddCatalogItemDialogComponent,
    EditCatalogItemDialogComponent
  ]
})
export class AdminModule { }
