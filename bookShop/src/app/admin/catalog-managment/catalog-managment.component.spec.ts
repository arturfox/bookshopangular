import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogManagmentComponent } from './catalog-managment.component';

describe('CatalogManagmentComponent', () => {
  let component: CatalogManagmentComponent;
  let fixture: ComponentFixture<CatalogManagmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogManagmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogManagmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
