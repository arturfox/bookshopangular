import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { MatDialog, MatCheckbox } from '@angular/material';
import { IAppState } from '../../core/store/state/app.state';
import { MAX_ITEMS_COUNT_PER_PAGE } from '../../../environments/environment';
import { GetCatalogModel } from '../../core/models/api/catalog-managment/get-catalog-model';
import { CatalogManagmentSortType } from '../../core/enums/catalog-managment-sort-type.enum';
import { GetCatalog, RemoveProduct } from '../../core/store/actions/catalog-managment.actions';
import { PrintingEditionType } from '../../core/enums/printing-edition-type.enum';
import { selectCatalog, selectPagination } from '../../core/store/selectors/catalog-managment.selectors';
import { CatalogModelItem } from '../../core/models/api/catalog-managment/catalog-model-item';
import { ActionDialogComponent } from '../../shared/components/action-dialog/action-dialog.component';
import { AddCatalogItemDialogComponent } from '../add-catalog-item-dialog/add-catalog-item-dialog.component';
import { EditCatalogItemDialogComponent } from '../edit-catalog-item-dialog/edit-catalog-item-dialog.component';

@Component({
  selector: 'app-catalog-managment',
  templateUrl: './catalog-managment.component.html',
  styleUrls: ['./catalog-managment.component.scss']
})
export class CatalogManagmentComponent implements OnInit {

  catalog$ = this._store.pipe(select(selectCatalog));
  displayedColumns: string[] = ['productId', 'name', 'description', 'category', 'author', 'price', 'actions'];

  filter: GetCatalogModel;

  pagination$ = this._store.pipe(select(selectPagination));

  //printing edition category filter
  @ViewChildren('categoryCheckbox') categoryCheckBoxes: QueryList<MatCheckbox>
  categories: { id: number; name: string }[] = [];

  pagination = {
    itemsPerPage: 0,
    currentPage: 0,
    totalItems: 0,
    id: "authormanagment"
  };

  constructor(private _store: Store<IAppState>,
    public dialog: MatDialog) {
    this.pagination$.subscribe((data) => {
      if (data === null) {
        return;
      }
      this.pagination = data;
    });

    for (var n in PrintingEditionType) {
      if (typeof PrintingEditionType[n] === 'number'
        && PrintingEditionType[n] != PrintingEditionType.None.toString()) {
        this.categories.push({ id: <any>PrintingEditionType[n], name: n });
      }
    }
  }

  ngOnInit() {
    this.filter = new GetCatalogModel();
    this.filter.page = 0;
    this.filter.takeItems = MAX_ITEMS_COUNT_PER_PAGE;
    this.filter.sortBy = CatalogManagmentSortType.NumberDesc;

    this._store.dispatch(new GetCatalog(this.filter));
  }

  onCreateClicked(){
    this.dialog.open(AddCatalogItemDialogComponent, {
      width: '45%',
      minWidth: '500px',
      maxWidth: '1200px'
    });
  }

  removeProduct(element: CatalogModelItem) {
    const dialogRef = this.dialog.open(ActionDialogComponent, {
      minWidth: '400px',
      data: { title: "Remove product", subTitle: `Are you sure you want to delete ${element.title} ?` }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._store.dispatch(new RemoveProduct(element.id));
      }
    });
  }

  editProduct(element: CatalogModelItem) {
    this.dialog.open(EditCatalogItemDialogComponent, {
      width: '45%',
      minWidth: '500px',
      maxWidth: '1200px',
      data: element
    });
  }

  handlePageChange(data: number) {
    this.filter = Object.assign({}, this.filter, {
      "page": data - 1
    });

    this._store.dispatch(new GetCatalog(this.filter));
  }
  
  onSelectCriteria(event: any) {
    let states = new Array<PrintingEditionType>();
    this.categoryCheckBoxes.forEach((element) => {
      if (!element.checked) {
        return;
      }
      states.push(PrintingEditionType[element.name]);
    });
    this.filter = Object.assign({}, this.filter, {
      "types": states
    });

    this._store.dispatch(new GetCatalog(this.filter));
  }

  onUserNameSortChanged() {
    let criteria = (this.filter.sortBy === CatalogManagmentSortType.NameAsc)
      ? CatalogManagmentSortType.NameDesc
      : CatalogManagmentSortType.NameAsc;

    this.updateCriteria(criteria);
  }

  onCatalogIdSortChanged() {
    let criteria = (this.filter.sortBy === CatalogManagmentSortType.NumberAsc)
      ? CatalogManagmentSortType.NumberDesc
      : CatalogManagmentSortType.NumberAsc;

    this.updateCriteria(criteria);
  }

  onPriceSortChanged() {
    let criteria = (this.filter.sortBy === CatalogManagmentSortType.PriceAsc)
      ? CatalogManagmentSortType.PriceDesc
      : CatalogManagmentSortType.PriceAsc;

    this.updateCriteria(criteria);
  }

  updateCriteria(criteria: CatalogManagmentSortType) {
    this.filter = Object.assign({}, this.filter, {
      "sortBy": criteria
    });

    this._store.dispatch(new GetCatalog(this.filter));
  }
}