import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IAppState } from '../../core/store/state/app.state';
import { GetOrders } from '../../core/store/actions/orders.actions';
import { selectOrders } from '../../core/store/selectors/orders.selectors';
import { OrderStatus } from '../../core/enums/order-status.enum';
import { Checkout } from '../../core/store/actions/orders.actions';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { CheckoutResultDialogComponent } from '../checkout-result-dialog/checkout-result-dialog.component';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.scss']
})
export class MyOrdersComponent implements OnInit {

  orders$ = this._store.pipe(select(selectOrders));
  displayedColumns: string[] = ['orderId', 'orderTime', 'product', 'title', 'quantity', 'orderAmount', 'orderStatus'];
  orderStatus: any = OrderStatus;

  constructor(private _store: Store<IAppState>,
    private _route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog) {

    this._route.params.subscribe(params => {
      let id = +params['id'];

      if (isNaN(id)) {
        return;
      }

      if (this.router.url.includes('payment-cancel')) {
        this.openResultDialog(id, false);
        return;
      }

      if (this.router.url.includes('payment-success')) {
        this.openResultDialog(id, true);
      }
    });
  }

  ngOnInit() {

    this._store.dispatch(new GetOrders());
  }

  payOrder(orderId: number) {
    this._store.dispatch(new Checkout(orderId));
  }

  openResultDialog(id: number, isSucceeded: boolean) {

    const dialogRef = this.dialog.open(CheckoutResultDialogComponent, {
      data: { orderId: id, isSucceeded: isSucceeded },
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.router.navigate(['../../'], { relativeTo: this._route });
    });

  }
}