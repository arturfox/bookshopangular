import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyOrdersComponent } from './my-orders/my-orders.component';

const routes: Routes = [
    { path: '', component: MyOrdersComponent},
    { path: 'payment-success/:id', component: MyOrdersComponent},
    { path: 'payment-cancel/:id', component: MyOrdersComponent}
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class OrdersRoutingModule { }