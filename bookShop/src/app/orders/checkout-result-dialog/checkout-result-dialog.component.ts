import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-checkout-result-dialog',
  templateUrl: './checkout-result-dialog.component.html',
  styleUrls: ['./checkout-result-dialog.component.scss']
})
export class CheckoutResultDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: {orderId: number, isSucceeded: boolean}) { }

  ngOnInit() {
  }
}
