import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutResultDialogComponent } from './checkout-result-dialog.component';

describe('CheckoutResultDialogComponent', () => {
  let component: CheckoutResultDialogComponent;
  let fixture: ComponentFixture<CheckoutResultDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckoutResultDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutResultDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
