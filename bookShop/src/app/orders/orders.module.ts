import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { OrdersRoutingModule } from './orders-routing,module';
import { MatTableModule, MatDialogModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { CheckoutResultDialogComponent } from './checkout-result-dialog/checkout-result-dialog.component';

@NgModule({
  declarations: [MyOrdersComponent, CheckoutResultDialogComponent],
  imports: [
    OrdersRoutingModule,
    CommonModule,
    SharedModule,
    MatTableModule,
    MatDialogModule
  ],
  entryComponents: [CheckoutResultDialogComponent]
})
export class OrdersModule { }
