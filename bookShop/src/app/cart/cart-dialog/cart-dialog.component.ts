import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Store, select } from '@ngrx/store';
import { IAppState } from '../../core/store/state/app.state';
import { hasPositions, selectCart } from '../../core/store/selectors/cart.selectors';
import { RemoveFromCart, Checkout } from '../../core/store/actions/cart.actions';
import { RemovePositionModel } from '../../core/models/api/cart/remove-position-model';

@Component({
  selector: 'app-cart-dialog',
  templateUrl: './cart-dialog.component.html',
  styleUrls: ['./cart-dialog.component.scss']
})
export class CartDialogComponent implements OnInit {

  cart$ = this._store.pipe(select(selectCart));

  hasPositions$ = this._store.pipe(select(hasPositions));

  displayedColumns: string[] = ['product', 'unitPrice', 'qty', 'orderAmount', 'actions'];

  constructor(public dialogRef: MatDialogRef<CartDialogComponent>,
    private _store: Store<IAppState>) 
    {

    
    }

  ngOnInit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSubmit(){
    this._store.dispatch(new Checkout());
  }

  removePosition(cartItemId: number){

    let data = new RemovePositionModel();
    data.cartItemId = cartItemId
    this._store.dispatch(new RemoveFromCart(data));
  }
}
