import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartDialogComponent } from './cart-dialog/cart-dialog.component';
import { MatButtonModule, MatDialogModule, MatIconModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatTableModule} from '@angular/material/table';

@NgModule({
  declarations: [CartDialogComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    FlexLayoutModule,
    MatTableModule,
    MatIconModule
  ],
  exports: [CartDialogComponent],
  entryComponents: [CartDialogComponent]
})
export class CartModule { }
