import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatMenu, MatDialog } from '@angular/material';
import { IAppState } from '../core/store/state/app.state';
import { Store, select } from '@ngrx/store';
import { selectRole } from '../core/store/selectors/account.selectors';
import { selectCartPositionsCount } from '../core/store/selectors/cart.selectors';
import { Logout } from '../core/store/actions/account.actions';
import { UserRole } from '../core/enums/user-role.enum';
import { Router } from '@angular/router';
import { CartDialogComponent } from '../cart/cart-dialog/cart-dialog.component';

@Component({
  selector: 'navbar-panel',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  roles: any = UserRole;

  selectRole$ = this._store.pipe(select(selectRole));
  selectCartPositionsCount$ = this._store.pipe(select(selectCartPositionsCount));

  @ViewChild(MatMenu, { static: false }) menu: MatMenu;

  constructor(private _store: Store<IAppState>,
    public dialog: MatDialog,
    private _router: Router) { }

  ngOnInit() {

  }

  onMyOrdersClicked() {
    this._router.navigateByUrl('orders');
  }

  onMyProfileClicked() {
    this._router.navigateByUrl('account');
  }

  onCatalogClicked() {
    this._router.navigateByUrl('catalog');
  }

  onOrderManagmentClicked() {
    this._router.navigateByUrl('dashboard/order-managment');
  }

  onCatalogManagmentClicked(){
    this._router.navigateByUrl('dashboard/catalog-managment');
  }

  onUserManagmentClicked(){
    this._router.navigateByUrl('dashboard/user-managment');
  }

  onAuthorManagmentClicked(){
    this._router.navigateByUrl('dashboard/author-managment');
  }

  onLogOutClicked(){
    this._store.dispatch(new Logout());
  }

  onCartClicked() {
    const dialogRef = this.dialog.open(CartDialogComponent, {
      width: '500px'
    });
  }
}