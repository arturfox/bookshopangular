import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrationComponent } from './registration.component';
import { RegistrationSuccessfulComponent } from './registration-successful/registration-successful.component';

const routes: Routes = [
    { path: '', component: RegistrationComponent },
    { path: 'sucessfully', component: RegistrationSuccessfulComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class RegistrationRoutingModule { }