import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { IAppState } from '../core/store/state/app.state';
import { Store } from '@ngrx/store';
import { CreateAccount } from '../core/store/actions/account.actions';
import { CreateAccountModel } from '../core/models/api/authentication/create-account-model';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  registrationFormGroup: FormGroup;

  constructor(private _formBuilder: FormBuilder,
              private _store: Store<IAppState>) {


  }

  ngOnInit() {

    this.registrationFormGroup = this._formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      userName: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });

  }

  onSubmit(){

    let model = new CreateAccountModel();
    model.userName = (this.registrationFormGroup.get('userName') as FormControl).value;
    model.firstName = (this.registrationFormGroup.get('firstName') as FormControl).value;
    model.lastName = (this.registrationFormGroup.get('lastName') as FormControl).value;
    model.email = (this.registrationFormGroup.get('email') as FormControl).value;
    model.password = (this.registrationFormGroup.get('password') as FormControl).value;
    model.confirmPassword = (this.registrationFormGroup.get('confirmPassword') as FormControl).value;
    
    this._store.dispatch(new CreateAccount(model));    
  }

}
