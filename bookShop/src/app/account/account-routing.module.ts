import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { MatButtonModule } from '@angular/material';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
    { path: '', redirectTo: 'profile' },
    { path: 'profile', component: ProfileComponent },
    { path: 'welcome', component: WelcomeComponent}
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        MatButtonModule
    ],
    exports: [
        RouterModule
    ]
})
export class AccountRoutingModule { }