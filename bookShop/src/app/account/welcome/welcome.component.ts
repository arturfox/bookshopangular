import { Component, OnInit } from '@angular/core';
import { IAppState } from '../../core/store/state/app.state';
import { Store, select } from '@ngrx/store';
import { selectAccount } from '../../core/store/selectors/account.selectors';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  account$ = this._store.pipe(select(selectAccount));

  constructor(private _store: Store<IAppState>) 
  { 
    
  }

  ngOnInit() {
  }

}
