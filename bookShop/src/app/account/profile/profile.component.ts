import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, Form } from '@angular/forms';
import { CustomValidators, ConfirmValidParentMatcher } from '../../core/validators/custom-validators';
import { Store, select, ActionsSubject } from '@ngrx/store';
import { IAppState } from '../../core/store/state/app.state';
import { GetProfile, UpdateProfile, EAccountActions } from '../../core/store/actions/account.actions';
import { selectProfile } from '../../core/store/selectors/account.selectors';
import { Subscription } from 'rxjs';
import { UpdateProfileModel } from '../../core/models/api/authentication/update-profile-model';
import { ProfileModel } from '../../core/models/api/authentication/profile-model';
import { ofType } from '@ngrx/effects';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {

  userFormGroup: FormGroup;

  profile$ = this._store.pipe(select(selectProfile));

  imagePlaceholder = './../../assets/perm_media-24px.svg';
  imageRef: string;
  isImageUpdated: boolean;

  subscription: Subscription;
  actionsSubscription = new Subscription();

  confirmValidParentMatcher = new ConfirmValidParentMatcher();
  isInEditMode: boolean = false;
  profile: ProfileModel;

  @ViewChild('profileForm', { static: false }) form: ElementRef<Form>;

  constructor(private _formBuilder: FormBuilder,
    private _store: Store<IAppState>,
    private actionsSubject: ActionsSubject) {
    this.imageRef = this.imagePlaceholder;
    this.subscription = this.profile$.subscribe((profile) => {
      if (profile == null || profile.imageSource == null) {
        return;
      }

      this.imageRef = profile.imageSource;
      this.profile = profile;
    });

    
    this.actionsSubscription = this.actionsSubject.pipe(
      ofType(EAccountActions.UpdateProfileSuccess))
      .subscribe(data => {
        this.isInEditMode = false;
        this.userFormGroup.disable();
      });
  }

  ngOnInit() {

    this.userFormGroup = this._formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      userName: ['', Validators.required],
      email: ['', Validators.required],
      passwordGroup: this._formBuilder.group({
        password: [''],
        confirmPassword: ['']
      }, { validator: CustomValidators.childrenEqual })
    });

    this.userFormGroup.disable();

    this._store.dispatch(new GetProfile());
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onEditPressed() {
    this.isInEditMode = true;
    this.userFormGroup.enable();
  }

  showPreview(event) {

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = rs => {

          const imgBase64Path = e.target.result;

          this.imageRef = imgBase64Path;
          this.isImageUpdated = true;
        };
      };

      reader.readAsDataURL(event.target.files[0]);
    }
  }

  onCancelClick() {

    this.isInEditMode = false;

    if (this.profile == null) {
      return;
    }

    this.imageRef = (this.profile.imageSource == null)
    ? this.imagePlaceholder : this.profile.imageSource ;

    this.userFormGroup.patchValue(this.profile);
    this.userFormGroup.disable();
  }

  onSubmit() {
    if (!this.userFormGroup.valid) {
      return;
    }

    let model = new UpdateProfileModel();
    model.firstName = this.userFormGroup.get('firstName').value;
    model.lastName = this.userFormGroup.get('lastName').value;
    model.userName = this.userFormGroup.get('userName').value;
    model.email = this.userFormGroup.get('email').value;
    model.password = this.userFormGroup.get(['passwordGroup', 'password']).value;
    model.confirmPassword = this.userFormGroup.get(['passwordGroup', 'confirmPassword']).value;

    if (this.isImageUpdated) {
      model.imageSource = this.imageRef;
    }

    this._store.dispatch(new UpdateProfile(model));
  }
}
