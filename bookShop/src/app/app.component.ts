import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bookShop';

  constructor(iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer) {

    iconRegistry.addSvgIcon(
      'profile_menu',
      sanitizer.bypassSecurityTrustResourceUrl('assets/account_circle-24px.svg'));
    iconRegistry.addSvgIcon(
      'cart',
      sanitizer.bypassSecurityTrustResourceUrl('assets/shopping_cart-white-18dp.svg'));
    iconRegistry.addSvgIcon(
      'remove_cart',
      sanitizer.bypassSecurityTrustResourceUrl('assets/remove_shopping_cart-24px.svg'));
    iconRegistry.addSvgIcon(
      'remove_user',
      sanitizer.bypassSecurityTrustResourceUrl('assets/highlight_off-24px.svg'));
    iconRegistry.addSvgIcon(
      'edit',
      sanitizer.bypassSecurityTrustResourceUrl('assets/create-24px.svg'));
      iconRegistry.addSvgIcon(
        'create',
        sanitizer.bypassSecurityTrustResourceUrl('assets/add_circle-24px.svg'));     
  }
}
