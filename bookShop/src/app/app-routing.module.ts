import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserRole } from './core/enums/user-role.enum';
import { AuthGuard } from './core/guards/auth.guard';
import { OpposedAuthGuard } from './core/guards/opposed-auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },

  //not authorized
  { path: 'login', loadChildren: () => import('./login/login.module').then(mod => mod.LoginModule), canActivate: [OpposedAuthGuard] },
  { path: 'createaccount', loadChildren: () => import('./registration/registration.module').then(mod => mod.RegistrationModule), canActivate: [OpposedAuthGuard] },
  { path: 'password_recovery', loadChildren: () => import('./password-recovery/password-recovery.module').then(mod => mod.PasswordRecoveryModule), canActivate: [OpposedAuthGuard] },

  //authorized
  { path: 'catalog', loadChildren: () => import('./printing-edition/printing-edition.module').then(mod => mod.PrintingEditionModule), canActivate: [AuthGuard] },
  { path: 'account', loadChildren: () => import('./account/account.module').then(mod => mod.AccountModule), canActivate: [AuthGuard] },

  //authorized - client
  { path: 'orders', loadChildren: () => import('./orders/orders.module').then(mod => mod.OrdersModule), canActivate: [AuthGuard], data: {roles: [UserRole.Client]} },
  
  //authorized - admin
  { path: 'dashboard', loadChildren: () => import('./admin/admin.module').then(mod => mod.AdminModule), canActivate: [AuthGuard], data: {roles: [UserRole.Admin]} }
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }