import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PasswordRecoveryComponent } from './password-recovery/password-recovery.component';
import { PasswordRecoveryRoutingModule } from './password-recovery-routing.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [PasswordRecoveryComponent],
  imports: [
    CommonModule,
    PasswordRecoveryRoutingModule,
    ReactiveFormsModule
  ]
})
export class PasswordRecoveryModule { }
