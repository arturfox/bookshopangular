import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { IAppState } from '../../core/store/state/app.state';
import { Store, select } from '@ngrx/store';
import { RecoverPasswordModel } from '../../core/models/api/authentication/recover-password-model';
import { RecoverPassword, ResetIsPasswordRecovered } from '../../core/store/actions/account.actions';
import { isPasswordRecovered } from '../../core/store/selectors/account.selectors';

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.component.html',
  styleUrls: ['./password-recovery.component.scss']
})
export class PasswordRecoveryComponent implements OnInit {

  isPasswordRecovered$ = this._store.pipe(select(isPasswordRecovered));

  recoveryForm = new FormGroup({
    email: new FormControl('')
  });

  constructor(private _store: Store<IAppState>) {

  }

  ngOnInit() {
  }

  onSubmit() {
    
    let model = new RecoverPasswordModel();
    model.email = (this.recoveryForm.get('email') as FormControl).value;

    this._store.dispatch(new RecoverPassword(model));
  }

  continue(){
    this._store.dispatch(new ResetIsPasswordRecovered());
  }
}
