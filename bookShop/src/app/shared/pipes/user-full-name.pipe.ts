import { Pipe, PipeTransform } from '@angular/core';
import { UsersModelItem } from '../../core/models/api/user-managment/users-model-item';

@Pipe({
  name: 'userFullName'
})
export class UserFullNamePipe implements PipeTransform {

  transform(user: UsersModelItem): string {

    if(user === null){
      return null;
    }
    
    return `${user.firstName} ${user.lastName}`;
  }
}
