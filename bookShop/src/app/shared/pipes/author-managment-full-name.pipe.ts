import { Pipe, PipeTransform } from '@angular/core';
import { AuthorsModelItem } from '../../core/models/api/author-managment/authors-model-item';

@Pipe({
  name: 'authorManagmentFullName'
})
export class AuthorManagmentFullNamePipe implements PipeTransform {
  
  transform(user: AuthorsModelItem): string {
    if(user === null){
      return null;
    }    
    return `${user.firstName} ${user.lastName}`;
  }

}
