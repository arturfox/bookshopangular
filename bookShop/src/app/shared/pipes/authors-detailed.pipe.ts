import { Pipe, PipeTransform } from '@angular/core';
import { AuthorModel } from '../../core/models/api/catalog/author-model';

@Pipe({
  name: 'authorsDetailed'
})
export class AuthorsDetailedPipe implements PipeTransform {

  transform(authors: AuthorModel[]): string {

    if(authors === null || authors.length === 0){
      return null;
    }

    let authorsList = `${authors[0].firstName} ${authors[0].lastName} (Author)`;

    if(authors.length == 1){
      return authorsList;
    }

    for (let i = 1; i < authors.length; i++) {

      authorsList = `${authorsList}, ${authors[i].firstName} ${authors[i].lastName} (Author)`
    }
    
    return authorsList;
  }

}
