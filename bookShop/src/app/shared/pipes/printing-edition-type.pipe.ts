import { Pipe, PipeTransform } from '@angular/core';
import { PrintingEditionType } from '../../core/enums/printing-edition-type.enum';

@Pipe({
  name: 'printingEditionType'
})
export class PrintingEditionTypePipe implements PipeTransform {

  transform(value: PrintingEditionType): string {

    return PrintingEditionType[value];
  }
}
