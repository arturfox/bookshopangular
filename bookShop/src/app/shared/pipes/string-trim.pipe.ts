import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringTrim'
})
export class StringTrimPipe implements PipeTransform {

  transform(text: string, length?: number): string {

    if (isNaN(length) || length <= 0 || text.length <= length) {

      return text;
    }

    return `${text.substring(0, length)}...`;
  }
}
