import { Pipe, PipeTransform } from '@angular/core';
import { Currency } from '../../core/enums/currency.enum';

@Pipe({
  name: 'price'
})
export class PricePipe implements PipeTransform {

  transform(value: number, currency: any): string {

    if (value === null) {
      return null;
    }

    if (currency === undefined || currency === Currency.None) {
      return value.toString();
    }

    let symbol = this.getSymbolByCurrencyType(currency);

    return `${symbol} ${value}`;
  }

  getSymbolByCurrencyType(currency: Currency): string {

    switch (currency) {
      case Currency.CHF: {
        return "₣";
      }
      case Currency.EUR: {
        return "€";
      }
      case Currency.GBP: {
        return "£";
      }
      case Currency.JPY: {
        return "¥";
      }
      case Currency.UAH: {
        return "₴";
      }
      case Currency.USD: {
        return "$";
      }
      default: {
        return ``;
      }
    }
  }

}
