import { Pipe, PipeTransform } from '@angular/core';
import { AccountStatus } from '../../core/enums/account-status.enum';

@Pipe({
  name: 'isAccountActive'
})
export class IsAccountActivePipe implements PipeTransform {

  transform(status: AccountStatus): boolean {    
    return (status === AccountStatus.Active);
  }
}

