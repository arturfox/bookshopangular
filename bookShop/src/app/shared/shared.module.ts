import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorFullNamePipe } from './pipes/author-full-name.pipe';
import { PricePipe } from './pipes/price.pipe';
import { AuthorsDetailedPipe } from './pipes/authors-detailed.pipe';
import { PrintingEditionTypePipe } from './pipes/printing-edition-type.pipe';
import { UserFullNamePipe } from './pipes/user-full-name.pipe';
import { IsAccountActivePipe } from './pipes/is-account-active.pipe';
import { ActionDialogComponent } from './components/action-dialog/action-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { AuthorManagmentFullNamePipe } from './pipes/author-managment-full-name.pipe';
import { StringTrimPipe } from './pipes/string-trim.pipe';
import { ImageUploadComponent } from './components/image-upload/image-upload.component';
import { ConnectFormDirective } from './directives/connect-form.directive';

@NgModule({
  declarations: [AuthorFullNamePipe, PricePipe, AuthorsDetailedPipe, PrintingEditionTypePipe, UserFullNamePipe, IsAccountActivePipe, ActionDialogComponent, AuthorManagmentFullNamePipe, StringTrimPipe, ImageUploadComponent, ConnectFormDirective],
  exports:[AuthorFullNamePipe, PricePipe, AuthorsDetailedPipe, PrintingEditionTypePipe, UserFullNamePipe, IsAccountActivePipe, AuthorManagmentFullNamePipe, StringTrimPipe, ImageUploadComponent, ConnectFormDirective],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule  
  ],
  entryComponents: [ActionDialogComponent]
})
export class SharedModule { }
