import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { LoginModel } from '../core/models/api/authentication/login-model';
import { IAppState } from '../core/store/state/app.state';
import { Store, select } from '@ngrx/store';
import { Login, LoginByReference } from '../core/store/actions/account.actions';
import { selectAccount } from '../core/store/selectors/account.selectors';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { LoginResultModel } from '../core/models/api/authentication/login-result-model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  account$ = this._store.pipe(select(selectAccount));

  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });

  constructor(private _store: Store<IAppState>,
    private _route: ActivatedRoute,
    private _router: Router) {

    this._route.queryParams.subscribe(queryParams => {
      if (queryParams['token'] !== undefined && queryParams['refreshtoken'] !== undefined) {
        
        let model = new LoginResultModel();
        model.token = queryParams['token'];
        model.refreshToken = queryParams['refreshtoken'];
        
        this._store.dispatch(new LoginByReference(model));
      }
    });

    this.account$.subscribe((account) => {
      if (account === null) {
        return;
      }

      if (account.isFirstLogin) {
        this._router.navigateByUrl('account/welcome');
        return;
      }

      this._router.navigateByUrl('catalog');
    });
  }

  ngOnInit() { }

  onSubmit() {

    let model = new LoginModel();
    model.userName = (this.loginForm.get('email') as FormControl).value;
    model.password = (this.loginForm.get('password') as FormControl).value;

    this._store.dispatch(new Login(model));
  }
}
